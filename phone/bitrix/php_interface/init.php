<?
define("LOG_FILENAME", "/var/www/bb.loc/public_html/bitrix/php_interface/logs/init1.logs");

// файл /bitrix/php_interface/init.php
// регистрируем обработчик
AddEventHandler("iblock", "OnAfterIBlockElementUpdate", Array("RelatedItem", "OnAfterIBlockElementUpdateHandler"));

\Bitrix\Main\EventManager::getInstance()->addEventHandlerCompatible(
    'sale',
    'OnSaleComponentOrderProperties',
    'SaleOrderEvents::fillLocation'
);

CModule::IncludeModule("iblock");


// Изменение локации в корзине
class SaleOrderEvents
{
    function fillLocation(&$arUserResult, $request, &$arParams, &$arResult)
    {
        $registry = \Bitrix\Sale\Registry::getInstance(\Bitrix\Sale\Registry::REGISTRY_TYPE_ORDER);
        $orderClassName = $registry->getOrderClassName();
        $order = $orderClassName::create(\Bitrix\Main\Application::getInstance()->getContext()->getSite());
        $propertyCollection = $order->getPropertyCollection();
        foreach ($propertyCollection as $property)
        {
            if ($property->isUtil())
                continue;

            $arProperty = $property->getProperty();
            if(
                $arProperty['TYPE'] === 'LOCATION'
                && array_key_exists($arProperty['ID'],$arUserResult["ORDER_PROP"])
                && !$request->getPost("ORDER_PROP_".$arProperty['ID'])
                && (
                    !is_array($arOrder=$request->getPost("order"))
                    || !$arOrder["ORDER_PROP_".$arProperty['ID']]
                )
            ){
                $arUserResult["ORDER_PROP"][$arProperty['ID']] = $GLOBALS['CODE']; // CURRENT_CITY_CODE;
            }
        }
    }
}


// Привязки товаров к друг другу по памяти, цвету. Привязка аксессуаров.
class RelatedItem
{
    // создаем обработчик события "OnAfterIBlockElementUpdate"
    public function OnAfterIBlockElementUpdateHandler(&$arFields)
    {
        if( $arFields["RESULT"] ){

            $iblock_id = $arFields['IBLOCK_ID'];
            $this_element_id = $arFields["ID"];

            $arr_fields_expert = array();
            $arr_fields_profi =  array();
            $arr_fields_kit =  array();

            $arr_fields_other_color =  array();
            $arr_fields_other_memory =  array();

            $arr_other_color = $arFields['PROPERTY_VALUES']['47'];  //Другие цвета 50
            $arr_other_memory = $arFields['PROPERTY_VALUES']['110']; // LINK_OTHER_MEMORY 61

            $arr_profi = $arFields['PROPERTY_VALUES']['48']; // Комплект Профи 51
            $arr_expert = $arFields['PROPERTY_VALUES']['49']; // Комплект Эксперт 52
            $arr_kit = $arFields['PROPERTY_VALUES']['50']; // Свой комплект 53

            $arr_gift = $arFields['PROPERTY_VALUES']['51']; // Подарок 54



            $arr_fields_other_color = self::create_arr( $arr_other_color , $this_element_id );
            $arr_fields_other_memory = self::create_arr( $arr_other_memory , $this_element_id );

            $arr_fields_expert = self::create_arr( $arr_expert );
            $arr_fields_profi = self::create_arr( $arr_profi );
            $arr_fields_kit = self::create_arr( $arr_kit );


            foreach ($arr_other_color as $k => $v){
                $element_id_color = $v['VALUE'];
                self::insert_property($element_id_color, $iblock_id , $arr_fields_expert, $arr_fields_profi ,  $arr_fields_kit);
                self::cross_link_element($element_id_color, $iblock_id , 'LINK_OTHER_COLOR' , $arr_fields_other_color );
                self::cross_link_element($element_id_color, $iblock_id , 'GIFT' , $arr_gift );
            }


            foreach ($arr_other_memory as $k => $v){
                $element_id_memory = $v['VALUE'];
                self::insert_property($element_id_memory, $iblock_id , $arr_fields_expert, $arr_fields_profi ,  $arr_fields_kit);
                self::cross_link_element($element_id_memory, $iblock_id , 'LINK_OTHER_MEMORY' , $arr_fields_other_memory );
                self::cross_link_element($element_id_memory, $iblock_id , 'GIFT' , $arr_gift );
            }

        }
        else{
            AddMessage2Log("Ошибка изменения записи \n ".$arFields["ID"]." (".$arFields["RESULT_MESSAGE"].").");
        }
    }


    public function insert_property($element_id, $iblock_id , $arr_fields_expert, $arr_fields_profi ,  $arr_fields_kit)
    {
        CIBlockElement::SetPropertyValuesEx( $element_id, $iblock_id,
            array( 'EXPERT' => $arr_fields_expert , 'PROFI' => $arr_fields_profi , 'KIT' => $arr_fields_kit ) );
    }


    public function cross_link_element($element_id, $iblock_id , $link_other_property , $arr_fields_link )
    {
        unset( $arr_fields_link[$element_id] );
        CIBlockElement::SetPropertyValuesEx( $element_id, $iblock_id,
            array( $link_other_property => $arr_fields_link  ) );
    }

    public function create_arr($arr, $this_element_id = false)
    {
        $arr_fields = array();
        foreach ($arr as $key => $val){
            $arr_fields[$val['VALUE']] = Array(
                "VALUE" => $val['VALUE']
            );
        }
        if ($this_element_id){
            $arr_fields[$this_element_id] = array( "VALUE" => $this_element_id) ;
        }
        return $arr_fields;
    }
    
    
}




?>