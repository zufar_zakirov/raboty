<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arCookieMessages = array(
	's1' => 'Сайт использует файлы cookies и сервис сбора технических данных его посетителей. Продолжая использовать данный ресурс, вы автоматически соглашаетесь с использованием данных технологий.',
	's2' => 'The site uses cookies and a service to collect technical data from its visitors. Continue to use this resource, you will automatically agree with the use of these technologies.'
);

if (empty($_COOKIE['wh-cookie-accept'])) : ?>
<div class="hide cookie-notify js-cookie-accept">
	<div class="cookie-notify__content"><?php print $arCookieMessages[SITE_ID]; ?> <button class="btn"><span>OK</span></button></div>
</div>
<?php endif; ?>