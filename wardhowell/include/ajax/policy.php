<?php
define("NO_KEEP_STATISTIC", true); 
define("NOT_CHECK_PERMISSIONS", true);

$_SERVER['DOCUMENT_ROOT'] = dirname(dirname(__DIR__));
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

$WEB_FORM_ID = 6;
$WEB_FORM_VIEW = "modal-policy";

if ( strpos($_SERVER["HTTP_REFERER"], '/en/') ){
	$WEB_FORM_ID = 8;
	$WEB_FORM_VIEW = "modal-policy-en";
}

$APPLICATION->IncludeComponent(
	"bitrix:form.result.new",
	$WEB_FORM_VIEW,
	Array(
		"CACHE_TIME" => "3600000",
		"CACHE_TYPE" => "A",
		"CHAIN_ITEM_LINK" => "",
		"CHAIN_ITEM_TEXT" => "",
		"EDIT_URL" => "",
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"LIST_URL" => "",
		"SEF_MODE" => "N",
		"SUCCESS_URL" => "",
		"USE_EXTENDED_ERRORS" => "N",
		"VARIABLE_ALIASES" => Array(
			"RESULT_ID" => "RESULT_ID",
			"WEB_FORM_ID" => "WEB_FORM_ID"
		),
        "WEB_FORM_ID" => $WEB_FORM_ID,
        "AJAX_MODE" => "Y",
        "AJAX_OPTION_SHADOW" => "N",
        "AJAX_OPTION_JUMP" => "N", 
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
	)
);