<?php

if (empty($_SERVER['HTTP_REFERER']) || false === strpos($_SERVER['HTTP_REFERER'], $_SERVER['REMOTE_HOST']) {
	exit;
}
if (empty($_POST['title']) || empty($_POST['html'])) {
	exit;
}

require($_SERVER["DOCUMENT_ROOT"] . "/include/vendor/autoload.php");

$dompdf = new Dompdf();

$dompdf->set_option('defaultFont', 'DejaVu Serif');
$dompdf->setPaper('A4', 'portrait');

$dompdf->loadHtml($_POST['html']);
$dompdf->render();

$dompdf->stream($_POST['title'].'.pdf', ['Attachment' => 0]);
