<?php
define("NO_KEEP_STATISTIC", true); 
define("NOT_CHECK_PERMISSIONS", true);


$_SERVER['DOCUMENT_ROOT'] = dirname(dirname(__DIR__));
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

if (empty($_REQUEST['email_message'])) {
	$_REQUEST['email_message'] = "«".$_REQUEST['title']."».\n".$_REQUEST['link'];
}

$fill_field = 'Заполните все обязательные поля.';
$enter_email = 'Введите e-mail адрес получателя.';
$close = 'Закрыть';
$send_article_to_email = 'Отправить материал на электронную почту';
$sent_successfully= 'Сообщение успешно отправлено';
$send = 'Отправить';
$email_recipient = 'Эл. почта получателя';

if ( strpos($_SERVER["HTTP_REFERER"], '/en/') ){
	$fill_field = 'Fill in all required fields.';
	$enter_email = 'Enter the recipient e-mail address.';
	$close = 'Close';
	$send_article_to_email = 'Send material to email';
	$sent_successfully = 'The message has been successfully sent';
	$send = 'Send';
	$email_recipient = 'Email recipient';
}

$sendStatus = false;
$errors = [];

do {
	if (false === strpos($_SERVER['HTTP_REFERER'], $_SERVER['SERVER_NAME'])) {
		break;
	}
	if (empty($_POST['action']) || 'send' != $_POST['action']) {
		break;
	}
	if (empty($_POST['email_to']) || empty($_REQUEST['email_message'])) {
		$errors['all'] = $fill_field;
		break;
	}
	if (!filter_var($_POST['email_to'], FILTER_VALIDATE_EMAIL)) {
		$errors['email_to'] = $enter_email;
	}
	
	if (empty($errors)) {
		$arEventFields = [];
		$arEventFields['EMAIL_FROM'] = 'from@wardhowell.com'; //$_POST['email_from'];
		$arEventFields['EMAIL_TO'] = $_POST['email_to'];
		$arEventFields['MESSAGE'] = $_REQUEST['email_message'];
            
        CEvent::SendImmediate('WH_SHARE', SITE_ID, $arEventFields, 'N');
		
		$sendStatus = true;
	}
} while (false);

?>
<div>
<div class="resume container">
<button type="button" data-fancybox-close class="fancybox-button fancybox-close-small" title="<?=$close?>">
	<svg class="svg svg-arow-right" width="30" height="30"><use xlink:href="/ico/sprite/sprite.svg#close"></use></svg>
</button>
<div class="resume__wrap">
	<h2><?=$send_article_to_email?></h2>	
	
	<? if ($sendStatus) : ?>
		<div><?=$sent_successfully?></div>
	<? else : ?>
		<form action="/include/ajax/share_email.php?fancybox=true" method="POST">
			<? if ($errors) : ?>
				<div class="resume__form-error"><p><? print implode('</p><p>', $errors) ?></p></div>
			<? endif; ?>
			<div class="resume__form">
				<input type="hidden" name="action" value="send" />
				<input type="hidden" name="title" value="<?=$_REQUEST['title']?>" />
				<input type="hidden" name="link" value="<?=$_REQUEST['url']?>" />
				<input type="text" class="resume__form-input resume__form-input--50" placeholder="<?=$email_recipient?> *" name="email_to" value="<?=@$_REQUEST['email_to']?>" />
				<div class="resume__form-btn">
					<button type="submit" class="btn"><span><?=$send?></span></button>
				</div>
			</div>
		</form>
	<? endif; ?>
</div>
</div>
</div>