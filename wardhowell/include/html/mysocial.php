<div class="mysocial">
            <div class="container">
                <div class="mysocial__wrap">
                    <h3>Мы в социальных сетях</h3>
                    <div class="mysocial__link">
                        <a href="https://www.facebook.com/WardHowellTalentEquity/" class="mysocial__facebook">
                            <svg class="svg svg-facebook" width="50" height="50">
                                <use xlink:href="/bitrix/templates/main_template/ico/sprite/sprite.svg#facebook"></use>
                            </svg>
                        </a>
                        <a href="http://t.me/tei_wardhowell" class="mysocial__telegram">
                            <svg class="svg svg-telegram" width="50" height="50">
                                <use xlink:href="/bitrix/templates/main_template/ico/sprite/sprite.svg#telegram"></use>
                            </svg>
                        </a>
                        <a href="https://www.youtube.com/channel/UCT38T9iXU-gYND8DUMOnCmw" class="mysocial__youtube">
                            <svg class="svg svg-youtube" width="50" height="50">
                                <use xlink:href="/bitrix/templates/main_template/ico/sprite/sprite.svg#youtube"></use>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
        </div>