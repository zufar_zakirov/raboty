    <div class="subscribe__link">
            <h3>Контакты для СМИ</h3>
            <ul>
                <li class="subscribe__link-telegram">
                <!-- /consultants/item/staroverova-olga -->
                <a href="javascript: void(0)">
                <i>
                <svg class="svg svg-user" width="50" height="50">
                <use xlink:href="/bitrix/templates/main_template_simple/ico/sprite/sprite.svg#user"></use></svg> </i><span>Матвеева Александра</span>
                </a>
                </li>
                <li class="subscribe__link-telegram"><a href="tel:+7 (495) 921 2901">
                <i><svg class="svg svg-call" width="50" height="50">
                <use xlink:href="/bitrix/templates/main_template_simple/ico/sprite/sprite.svg#call"></use></svg> </i>
                <span>+7 (495) 921 2901 (доб. 1185)</span></a>
                </li>
                <li class="subscribe__link-telegram"><a href="mail:info@wardhowell.com"><i><svg class="svg svg-envelope" width="50" height="50">
                <use xlink:href="/bitrix/templates/main_template_simple/ico/sprite/sprite.svg#envelope"></use></svg> </i><span>a.matveeva@wardhowell.com</span></a>
                </li>
            </ul>
</div>