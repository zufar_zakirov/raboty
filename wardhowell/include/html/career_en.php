<div class="regwh">
	<div class="container">
		<div class="regwh__grid">
			<div class="regwh__title">
                        <h3>Interested in a career at Ward Howell?</h3>
                        <a href="/about/career.php" class="btn-arrow"><span>Career at Ward Howell</span> 
                        <i><svg class="svg svg-arrow" width="50" height="50"><use xlink:href="/bitrix/templates/main_template_simple/ico/sprite/sprite.svg#arrow"></use></svg></i></a>
                    </div>
			<div class="regwh__info">
				<p>
					We help our clients identify which leaders they need, find and engage leaders, and ensure their successful integration.
				</p>
			</div>
		</div>
	</div>
</div>