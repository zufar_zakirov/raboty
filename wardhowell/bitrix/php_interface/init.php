<?php

use Bitrix\Main;
use Bitrix\Main\Loader;
use DiDom\Document;
use Dompdf\Dompdf;

$eventManager = Main\EventManager::getInstance();
$eventManager->addEventHandler('main', 'OnEndBufferContent', ['CMainHandlers', 'onEndBufferContent']);
$eventManager->addEventHandler('main', 'OnBeforeEventSend', ['CMainHandlers', 'onBeforeEventSend']);

class CMainHandlers
{
    public static function onEndBufferContent(&$content)
    {
		if (!empty($_GET['pdf'])) {
			global $APPLICATION;
			
			$html = '';
			
			require($_SERVER["DOCUMENT_ROOT"] . "/include/vendor/autoload.php");

			$document = new Document($content);
			
			$panel = $document->find('.panel-article');
			if ($panel) {
				$panel[0]->remove();
			}
			$slider = $document->find('.article-slider__min');
			if ($slider) {
				$slider[0]->remove();
			}
			
			$blocks = $document->find('.block-page__content');
			foreach($blocks as $block) {
				$html .= $block->html();
			}
			unset($document, $blocks);
			
			$html = str_replace('img src="/', 'img src="'.$_SERVER["DOCUMENT_ROOT"].'/', $html);
			
			if ($html) {
				$html .= '<style>li>*{margin:0;} table{border:1px solid black;} img {height:auto;width:100%;}</style>';
				
				$dompdf = new Dompdf();

				$dompdf->set_option('defaultFont', 'DejaVu Serif');
				$dompdf->setPaper('A4', 'portrait');
				
				$dompdf->loadHtml($html);
				$dompdf->render();

				$dompdf->stream($APPLICATION->GetTitle().'.pdf', ['Attachment' => 0]);
				exit;
			}
		}
    }
	public static function onBeforeEventSend(&$arFields, $arTemplate)
	{
		if ('FORM_FILLING_SIMPLE_FORM_6' == $arTemplate['EVENT_NAME'] OR 'FORM_FILLING_SIMPLE_FORM_8' == $arTemplate['EVENT_NAME'] ) {
			$arFields['COPY_TO'] = '';
			if (! empty($arFields['SIMPLE_QUESTION_337']) && '' != trim($arFields['SIMPLE_QUESTION_337'])) {
				$arFields['COPY_TO'] = $arFields['SIMPLE_QUESTION_100'];
			}
		}

		// if ( 'FORM_FILLING_SIMPLE_FORM_8' == $arTemplate['EVENT_NAME'] ) {
		// 	$arFields['COPY_TO'] = '';
		// 	if (! empty($arFields['SIMPLE_QUESTION_337']) && '' != trim($arFields['SIMPLE_QUESTION_337'])) {
		// 		$arFields['COPY_TO'] = $arFields['SIMPLE_QUESTION_100'];
		// 	}
		// }

	}
}