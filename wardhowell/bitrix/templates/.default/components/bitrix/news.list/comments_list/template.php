<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<? if (  count ( $arResult["ITEMS"] ) >0 ): ?>


<div class="case">
    <div class="container">
        <div class="case__wrap">
            <div class="case__header">
                <h2><?=$arParams['PAGER_TITLE']?></h2>
                <div class="case__arrow">
                    <div class="case__arrow-left"><i><svg class="svg svg-arow-left" width="50" height="50"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/ico/sprite/sprite.svg#arow-left"></use></svg></i>
                    </div>
                    <div class="case__arrow-right"><i><svg class="svg svg-arow-right" width="50" height="50"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/ico/sprite/sprite.svg#arow-right"></use></svg></i>
                    </div>
                </div>
            </div>

<div class="case-slider js__case-slider">    



<?
$i = 1;
foreach($arResult["ITEMS"] as $arItem):?>
<?
$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

$disp_prop = $arItem["DISPLAY_PROPERTIES"];
?>


<? if ($i==1 ): ?>
<div class="case-slider__item">
            <div class="case-slider__grid">
<? endif; ?>

<? if ($i==1 ): ?>
                <div class="case-slider__grid-top">
<? endif; ?>

<? if ($i==3 ): ?>
                <div class="case-slider__grid-bottom">
<? endif; ?>
                    <div class="case-slider__grid-item">
                        <h3><?echo $arItem["NAME"]?></h3>
                        <p><?=$disp_prop['TEXT']['~VALUE']['TEXT']?></p>                        
                    </div>                    
<? if ( $i==2 OR  $i==4): ?>
                </div>
<? endif; ?>
                
<? if ($i==4 ): ?>
            </div>
</div>
<? endif; ?>
<?
$i++;
if ($i > 4){
    $i = 1;
}
endforeach;?>

</div>
</div>
</div>
</div>
<? endif; ?>