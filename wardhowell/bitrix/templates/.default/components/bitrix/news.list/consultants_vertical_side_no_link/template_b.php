<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="block-page__slider">
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>

<a class="consitem" href="<?=$arItem['DETAIL_PAGE_URL']?>">
    <i class="consitem__avatar">
    <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="IMG"></i>
        <div class="consitem__info">
            <h4><?=$arItem['NAME']?></h4>
            <p><?=$arItem['DISPLAY_PROPERTIES']['JOB_POSITION']["VALUE"]?></p>
            <p><? //=$arItem['DISPLAY_PROPERTIES']['SPECIALIZATION']["VALUE"]?>
            </p>
        </div>
</a>
<?endforeach;?>
</div>
