<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="media krelease-media">
    <div class="container">
        <div class="media__wrap">
            <h2>Видео интерью с Аламниями</h2>
            <div class="media__grid">


<?foreach($arResult["ITEMS"] as $arItem):?>
    <?
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

//var_dump( $arItem["FIELDS"]);

if ($arItem["FIELDS"]["TAGS"] !=''){
    $tags_arr = explode(',', $arItem["FIELDS"]["TAGS"]);
}

?>
                
                <div class="consitem consitem--news">
                <i class="consitem__avatar">
<?if ( !empty($tags_arr) ): ?>
                    <div class="consitem__label">
                    <?php foreach ($tags_arr as $key => $value): ?>
                        <span><?=$value?></span>
                        <?php endforeach ?>
                        
                    </div>
<?endif;?>
                    <img src="<?=$arItem["DISPLAY_PROPERTIES"]["IMAGE"]["FILE_VALUE"]["SRC"]?>" alt="IMG">
                   <!--  <div class="consitem__title">
                        <span>Harward Business Review</span>
                    </div> -->
                </i>
                    <div class="consitem__info">
                    <a href="#fhj"><h4><?=$arItem["NAME"]?></h4></a>
                        <p>
                                <?=$arItem["DISPLAY_PROPERTIES"]["TEXT"]["VALUE"]['TEXT']?>
                        </p>
                        <a href="#more" class="consitem__more"><i><svg class="svg svg-arrow" width="50" height="50">
                        <use xlink:href="/bitrix/templates/main_template/ico/sprite/sprite.svg#arrow"></use></svg></i></a>
                    </div>
                </div>
<?endforeach;?>


                </div>
            </div>
            <div class="media__more"><a href="#awdwa" class="btn"><span>Посмотреть еще</span></a>
            </div>
        </div>
</div>