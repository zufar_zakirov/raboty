<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<div class="article-search">
                <div class="container">
                    <div class="article-search__input">
                        <!-- <form class="article-search__input-form">
                            <input type="text" placeholder="Поиск по материалам"> <i><svg class="svg svg-search" width="50" height="50"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/ico/sprite/sprite.svg#search"></use></svg></i>
                        </form> -->
                    </div>
                    <div class="article-search__grid js-is-container">


<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
$class_consitem_video  = '';
$data_fancybox_video = '';
if ( $arItem["DISPLAY_PROPERTIES"]["VIDEO"]["VALUE"] != '' ){
	$data_fancybox_video = 'data-fancybox data-src="'.  $arItem["DISPLAY_PROPERTIES"]["VIDEO"]["VALUE"] .'"';
	$class_consitem_video = 'consitem--video';
}
	?>

<?php
if ($arItem["FIELDS"]["TAGS"] !=''){
	$tags_arr = explode(',', $arItem["FIELDS"]["TAGS"]);
}

$renderImage = CFile::ResizeImageGet( $arItem["DISPLAY_PROPERTIES"]["IMAGE_COVER"]["FILE_VALUE"]["ID"]
        , Array("width" => 371, "height" => 238), BX_RESIZE_IMAGE_EXACT, false);

?>


<div class="consitem consitem--news js-is-container-item <?=$class_consitem_video?>">
<i class="consitem__avatar" <?=$data_fancybox_video?>>
	<div class="consitem__label">
		<?php foreach ($tags_arr as $key => $value): ?>
	    		<a href="<?=SITE_DIR?>tags/?tag=<?=$value?>"><?=$value?></a> 
	    	<?php endforeach ?>
	</div>
	<img src="<?=$renderImage["src"]?>" class='df' alt="">
</i>
<div class="consitem__info">
<a href="<?echo $arItem["DETAIL_PAGE_URL"]?>">
    <h4><?echo $arItem["NAME"]?></h4>
</a>
        <p><?echo $arItem["PREVIEW_TEXT"];?></p>
<a href="<?echo $arItem["DETAIL_PAGE_URL"]?>"><i><svg class="svg svg-arrow" width="50" height="50">
        <use xlink:href="<?=SITE_TEMPLATE_PATH?>/ico/sprite/sprite.svg#arrow"></use></svg></i></a>
    </div>
</div>



	
<?endforeach;?>
</div>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?>
<?endif;?>
</div>
</div>
