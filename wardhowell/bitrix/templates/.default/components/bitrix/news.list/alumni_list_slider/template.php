<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>



        <div class="case">
            <div class="container">
                <div class="case__wrap">
                    <div class="case__header">
                        <h2>Наши Аламнии</h2>
                        <div class="case__arrow case__arrow--desktop">
                            <div class="case__arrow-left"><i><svg class="svg svg-arow-left" width="50" height="50"><use xlink:href="/bitrix/templates/main_template/ico/sprite/sprite.svg#arow-left"></use></svg></i>
                            </div>
                            <div class="case__arrow-right"><i><svg class="svg svg-arow-right" width="50" height="50"><use xlink:href="/bitrix/templates/main_template/ico/sprite/sprite.svg#arow-right"></use></svg></i>
                            </div>
                        </div>
                    </div>
                    <div class="case-slider case-slider--mobile-shadow js__case-slider">
<?foreach($arResult["ITEMS"] as $arItem):?>
    <?
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
                        <div class="case-slider__item">
                            <div class="case-slider__grid">
                                <div class="prof-team">
                                    <div class="prof-team__img">
                                        <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="IMG">
                                    </div>
                                    <div class="prof-team__info">
                                        <h3><?echo $arItem["NAME"]?></h3>
                                        <p>
                                            <?=$arItem["DISPLAY_PROPERTIES"]["POSITION"]["VALUE"]?>
                                        </p>
                                        <b><?=$arItem["DISPLAY_PROPERTIES"]["JOB"]["VALUE"]?></b>
                                        <p>
                                            <?=$arItem["DISPLAY_PROPERTIES"]["TEXT"]["VALUE"]['TEXT']?>
                                        </p><a href="#awdawd" class="btn-arrow"><span>Посмотреть интервью</span> <i><svg class="svg svg-arrow" width="50" height="50"><use xlink:href="/bitrix/templates/main_template/ico/sprite/sprite.svg#arrow"></use></svg></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>              
<?endforeach;?>
                        
                    </div>
                </div>
            </div>
        </div>