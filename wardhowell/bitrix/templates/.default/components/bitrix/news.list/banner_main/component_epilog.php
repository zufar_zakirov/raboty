<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

\Bitrix\Main\Page\Asset::getInstance()->addCSS('/bitrix/templates/.default/vendor/jquery-background-video-master/jquery.background-video.css');
\Bitrix\Main\Page\Asset::getInstance()->addJs('/bitrix/templates/.default/vendor/jquery-background-video-master/jquery.background-video.js');

?>
<?$APPLICATION->IncludeComponent(
    "bitrix:menu", 
    "header", 
    array(
        "ROOT_MENU_TYPE" => "front_page",
        "MENU_CACHE_TYPE" => "Y",
        "MENU_CACHE_TIME" => "36000000",
        "MENU_CACHE_USE_GROUPS" => "Y",
        "MENU_CACHE_GET_VARS" => array(
        ),
        "MAX_LEVEL" => "1",
        "CHILD_MENU_TYPE" => "",
        "USE_EXT" => "N",
        "ALLOW_MULTI_SELECT" => "N",
        "COMPONENT_TEMPLATE" => "header",
        "DELAY" => "N"
    ),
    false
);?>
</header>