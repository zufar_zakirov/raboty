<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$count = 0;
?>



<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
$class_consitem_video  = '';
$data_fancybox_video = '';
if ( $arItem["DISPLAY_PROPERTIES"]["VIDEO"]["VALUE"] != '' ){
	$data_fancybox_video = 'data-fancybox data-src="'.  $arItem["DISPLAY_PROPERTIES"]["VIDEO"]["VALUE"] .'"';
	$class_consitem_video = 'consitem--video';
}
	?>

<?php
$count++;
?>

<a href="<?echo $arItem["DETAIL_PAGE_URL"]?>" class="block-page__popular-item">
<b><?=$count?></b> 
<span><?echo $arItem["NAME"]?><i>
<svg class="svg svg-arrow" width="50" height="50">
<use xlink:href="<?=SITE_TEMPLATE_PATH?>/ico/sprite/sprite.svg#arrow"></use></svg> </i></span></a>
	
<?endforeach;?>