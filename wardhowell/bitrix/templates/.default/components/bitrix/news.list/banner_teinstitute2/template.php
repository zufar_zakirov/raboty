<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="material block-page">


<?
foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
//$arItem["PREVIEW_PICTURE"]

$cl_jarallax = '';
if ( !empty( $arItem["DISPLAY_PROPERTIES"]['BANNER']["FILE_VALUE"]["SRC"] ) )
{
    $cl_jarallax = ' jarallax" style="width:100%; background-image: url( \'' . $arItem["DISPLAY_PROPERTIES"]['BANNER']["FILE_VALUE"]["SRC"] .'\' );"';
}


	?>
<div class="material__header jarallax" style="background-image: url(<?=$arItem["DISPLAY_PROPERTIES"]["BANNER"]["FILE_VALUE"]["SRC"]?>)">
    <div class="headslider__content container">
        <h2><?echo $arItem["NAME"]?></h2>
        <a href="<?=$arItem["DISPLAY_PROPERTIES"]["URL"]["VALUE"]?>" class="btn btn--head"><span>
        <?=$arItem["DISPLAY_PROPERTIES"]["TEXT_HREF"]["VALUE"]?>
        </span></a>
    </div>
</div>
<?endforeach;?>


</div>