<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
$renderImage = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"]['ID'], Array("width" => 510, "height" => 600), BX_RESIZE_IMAGE_PROPORTIONAL, false); 
	?>



                       <div class="case-slider__item">
                            <div class="case-slider__grid">
                                <div class="prof-team">
                                    <div class="prof-team__img">
                                        <img src="<?=$renderImage["src"]?>" alt="IMG">
                                    </div>
                                    <div class="prof-team__info">
                                        <h3><?echo $arItem["NAME"]?></h3>
                                        <p><?=$arItem["DISPLAY_PROPERTIES"]["JOB_POSITION"]["VALUE"]?></p>
                                        <p>

                                            <?
                                                echo TruncateText($arItem["~DETAIL_TEXT"], 470);
                                            ?>
                                        </p>

                                            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="btn-arrow"><span>Подробнее</span> <i><svg class="svg svg-arrow" width="50" height="50"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/ico/sprite/sprite.svg#arrow"></use></svg></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>







 
	
<?endforeach;?>