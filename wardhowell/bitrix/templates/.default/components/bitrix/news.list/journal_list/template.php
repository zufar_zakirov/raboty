<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="allnews__grid">

<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>



<div class="allnews__grid-item">
    <div class="journal">
        <div class="journal__ico">
        <a href="<?=$arItem['DETAIL_PAGE_URL']?>">
            <i>
            <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="IMG"></i>
        </a>
        </div>
        <div class="journal__footer">
        <a href="<?=$arItem["DISPLAY_PROPERTIES"]["PDF_JOURNAL"]["FILE_VALUE"]["SRC"];?>" class="journal__download"><? echo GetMessage('READ_JOURNAL_ONLINE'); ?></a> 
            <a href="<?=$arItem["DISPLAY_PROPERTIES"]["PDF_JOURNAL"]["FILE_VALUE"]["SRC"];?>" class="journal__pdf">
                <svg class="svg svg-pdf" width="50" height="50">
                    <use xlink:href="<?=SITE_TEMPLATE_PATH?>/ico/sprite/sprite.svg#pdf"></use>
                </svg>
            </a>            
            <a href="<?=$arItem['DETAIL_PAGE_URL']?>#print" class="journal__printer">
                <svg class="svg svg-printer" width="50" height="50">
                    <use xlink:href="<?=SITE_TEMPLATE_PATH?>/ico/sprite/sprite.svg#printer"></use>
                </svg>
            </a>
            <a href="#" class="journal__envelope js-share-email" data-title="<?=htmlspecialchars($arItem['NAME'])?>" data-url="<?=$arItem['DETAIL_PAGE_URL']?>">
                <svg class="svg svg-envelope" width="50" height="50">
                    <use xlink:href="<?=SITE_TEMPLATE_PATH?>/ico/sprite/sprite.svg#envelope"></use>
                </svg>
            </a>
        </div>
    </div>
</div>




<?endforeach;?>

</div>
