<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>


<div class="consult__slider-item">
<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="consitem">
    
    <i class="consitem__avatar">        
            <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="IMG">
    </i>
            <div class="consitem__info">
                <h4><?echo $arItem["NAME"]?></h4>
                <p><?=$arItem["DISPLAY_PROPERTIES"]["JOB_POSITION"]["VALUE"]?></p>
                <p><? //=$arItem['DISPLAY_PROPERTIES']['SPECIALIZATION']['VALUE']; ?></p>
                   <div class="consitem__info-arrow" href="#more">
                    <i><svg class="svg svg-arrow" width="50" height="50">
                    <use xlink:href="<?=SITE_TEMPLATE_PATH?>/ico/sprite/sprite.svg#arrow"></use></svg></i>
                </div>
        </div>    
    </a>
</div>
	
<?endforeach;?>