<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

$renderImage = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"]["ID"], Array("width" => 101, "height" => 101), BX_RESIZE_IMAGE_EXACT, false); 
	?>

<div class="block-page__author-item block">
	<a href="<?=$arItem['DETAIL_PAGE_URL']?>">    
	    	<img src="<?=$renderImage['src']?>" align="left">
	            <p>        
	            		<b><?=$arItem['NAME']?></b><br />
	            		<?=$arItem['DISPLAY_PROPERTIES']['JOB_POSITION']["~VALUE"]?>
	            </p>
	</a>
        
</div>
<?endforeach;?>

