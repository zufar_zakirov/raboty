<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? if ( count( $arResult["ITEMS"] ) >0 ): 
$i = 1;
?>


<div class="school-reviews__add">
                        <div class="school-reviews__slider">

<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

	$disp_prop = $arItem["DISPLAY_PROPERTIES"];
	?>

<div class="school-reviews__item s-review">
    <div class="s-review__text">
        <p><?echo substr( $arItem["DETAIL_TEXT"] , 0 , 450) ;?></p>
    </div>
    <div class="s-review__author">
        <img class="s-review__userpic" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"] ?>" alt="">
    <div><strong><? echo $arItem['~NAME'] ;?></strong>  <span><?=$disp_prop['HEAD']['~VALUE']?></span>
        </div>
    </div>
</div>	
	
<? $i++; endforeach;?>

</div>
</div>

<? endif; ?>