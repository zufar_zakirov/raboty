<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="consultants block-page">
<div class="container">
<div class="consultants__wrap">



<div class="consultants__items">
<?foreach($arResult["ITEMS"] as $arSecElItem):?>

	<? if(!empty($arSecElItem['ELEMENTS'])):?>
		
		<h2><?//echo $arSecElItem['NAME']?></h2>
		<div class="consultants__grid">

		<? if(!empty($arSecElItem['ELEMENTS'])):?>			
			<? foreach($arSecElItem['ELEMENTS'] as $arItem):?>
			
			<?
			if(!empty($arItem["PREVIEW_PICTURE"]["SRC"])){
				$image = $arItem["PREVIEW_PICTURE"]["SRC"];
			}else{
				$image = SITE_TEMPLATE_PATH."/images/nophoto.png";
			}
			?>

			<a class="consitem" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
				<i class="consitem__avatar">
				<img src="<?=$image?>" alt="IMG"></i>
	                                	<div class="consitem__info">
		                                    	<h4><?echo $arItem['NAME']?></h4>
		                                    	<p><?=$arItem['DISPLAY_PROPERTIES']['JOB_POSITION']['VALUE']; ?></p>	
		                                    	<p><?=$arItem['DISPLAY_PROPERTIES']['SPECIALIZATION']['VALUE']; ?></p>
		                                    	<div class="consitem__info-arrow">
		                                    		<i><svg class="svg svg-arrow" width="50" height="50">
		                                    		<use xlink:href="<?=SITE_TEMPLATE_PATH?>/ico/sprite/sprite.svg#arrow"></use>
		                                    		</svg></i>
		                                    	</div>
	                                    	</div>
	                            </a>
			<? endforeach; ?>
		<? endif; ?>
		</div>
	<? endif; ?>
<?endforeach;?>
</div>
</div>
</div>
</div>