<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="news">
    <div class="container">
        <div class="news__wrap">
            <h2><?=GetMessage("BLOCK_TITLE")?></h2>
            <div class="newsslider js__newsslider">



<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
$class_consitem_video  = '';
$data_fancybox_video = '';
if ( $arItem["DISPLAY_PROPERTIES"]["VIDEO"]["VALUE"] != '' ){
	$data_fancybox_video = 'data-fancybox data-src="'.  $arItem["DISPLAY_PROPERTIES"]["VIDEO"]["VALUE"] .'"';
	$class_consitem_video = 'consitem--video';
}


	?>


<?php
if ($arItem["FIELDS"]["TAGS"] !=''){
	$tags_arr = explode(',', $arItem["FIELDS"]["TAGS"]);
}

$renderImage = CFile::ResizeImageGet( $arItem["DISPLAY_PROPERTIES"]["IMAGE_COVER"]["FILE_VALUE"]["ID"]
        , Array("width" => 371, "height" => 238), BX_RESIZE_IMAGE_EXACT, false);         
?>


<div class="newsslider__item">
    <a class="consitem consitem--news <?=$class_consitem_video?>" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
    <i class="consitem__avatar" <?=$data_fancybox_video?> >
    <?if ( !empty($tags_arr) ): ?>
    	<em class="consitem__label">
	    	<?php foreach ($tags_arr as $key => $value): ?>
	    		<span><?=$value?></span> 
	    	<?php endforeach ?>    
    	</em>
    <?endif;?>

        <img src="<?=$renderImage["src"]?>" class="sdf" alt=""></i>
        <div class="consitem__info">
            <h4><?echo $arItem["NAME"]?></h4>
            <p><?echo $arItem["PREVIEW_TEXT"];?></p>

            <div class="consitem__info-arrow"><i><svg class="svg svg-arrow" width="50" height="50"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/ico/sprite/sprite.svg#arrow"></use></svg></i></div>
        </div>
    </a>
</div>

<?endforeach;?>

</div>
</div>
</div>
</div>
