<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>
<div class="article-search__input">
<form action="<?=$arResult["FORM_ACTION"]?>" class="article-search__input-form">
<?if($arParams["USE_SUGGEST"] === "Y"):?>
<?$APPLICATION->IncludeComponent(
		"bitrix:search.suggest.input",
		"",
		array(
			"NAME" => "q",
			"VALUE" => "",
			"INPUT_SIZE" => 15,
			"DROPDOWN_SIZE" => 10,
		),
		$component, array("HIDE_ICONS" => "Y")
);?>
<?else:?>
<input type="text" name="q" value="" size="15" maxlength="50" class="js__menu-search-input" />
<?endif;?>
	<i><svg class="svg svg-search" width="50" height="50">
	<use xlink:href="/bitrix/templates/main_template_simple/ico/sprite/sprite.svg#search"></use></svg></i>

</form>
</div>