<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<div class="regevent">
<div class="container">
<div class="regevent__wrap">
<h3><?=$arResult["FORM_TITLE"]?></h3>
<? if (!empty($arResult['FORM_DESCRIPTION'])) : ?>
<p><?= $arResult['FORM_DESCRIPTION'] ?></p>
<? endif; ?>
<?php 
if ($arResult["isFormNote"] == "Y"): ?>
    <div class="callout success" style="color: #fff;"><?=GetMessage('THANKS');?></div>
<?php else: ?>
	<?=$arResult["FORM_HEADER"]?>
	<?
	foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion) {
		if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'hidden') {
			print $arQuestion["HTML_CODE"];
		}
	}
	?>
	<?if ($arResult["isFormErrors"] == "Y"):?>
        <?=$arResult["FORM_ERRORS_TEXT"]?>
    <?endif;?>
    <div class="regevent__form">
        <?
        foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion) : 
            $label = $arQuestion["CAPTION"].($arQuestion["REQUIRED"] == "Y" ? ' *' : '');
            $isError = !(empty($arResult['FORM_ERRORS_TEXT']) || false === strpos($arResult['FORM_ERRORS_TEXT'], $arQuestion["CAPTION"]));
        ?>
            <? if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'text' || $arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'email'): ?>                        
                <?
                //var_dump( $arQuestion['STRUCTURE'][0]['ID'] );
                        if ( 64 == $arQuestion['STRUCTURE'][0]['ID'] OR 65 == $arQuestion['STRUCTURE'][0]['ID'] )
                        {
                            print str_replace('value=""', 'value="'. $arParams['CURRENT_LINK'] .'" ', $arQuestion["HTML_CODE"]); 

                        }else{
		  print str_replace('class="inputtext"', 'class="" placeholder="'.$label.'"', $arQuestion["HTML_CODE"]); 
                        }
                ?>


            <? elseif ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'textarea') : ?>
				<script>
				function auto_grow(element)
				{
					element.style.height = "10px";
					element.style.height = (element.scrollHeight)+"px";
                }
                </script>
                <?
					print str_replace('class="inputtextarea"', ' class="resume__form-textarea" onkeyup="auto_grow(this)" placeholder="'.$label.'"', $arQuestion["HTML_CODE"]);
                ?>
			<? elseif ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'file') : ?>
				<div class="input-file" data-input-file="">
                <?
					print $arQuestion["HTML_CODE"];
                ?>
					<svg class="svg svg-attachment" width="50" height="50"><use xlink:href="/ico/sprite/sprite.svg#attachment"></use></svg> 
					<span data-input-file-selected=""><?= $arQuestion["CAPTION"] ?></span>
                </div>
            
            <? endif; ?>
        <?endforeach;?>
<button type="submit" class="btn" name="web_form_submit" value="<?=htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]);?>"><span><?=htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]);?></span></button>		


<?
foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion) : 
    $label = $arQuestion["CAPTION"].($arQuestion["REQUIRED"] == "Y" ? ' *' : '');
    $isError = !(empty($arResult['FORM_ERRORS_TEXT']) || false === strpos($arResult['FORM_ERRORS_TEXT'], $arQuestion["CAPTION"]));
?>
    <? if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'checkbox') : ?>
                    <div class="block-check">                    
                        <?
                            print str_replace('<label for', '<label for="'. $arQuestion["STRUCTURE"][0]["ID"]  .'" class="block-check__box"></label><label class="block-check__text " for', $arQuestion["HTML_CODE"]); 
                        ?>
                    </div>
    <? endif; ?>

    <? if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'hidden') : ?>
    df
        <?php echo $arQuestion["HTML_CODE"]; ?>
    <? endif; ?>



<?endforeach;?>

<input type="hidden" name="" value="<?=$arParams['CURRENT_LINK']?>">
<input type="hidden" name="" value="<?$APPLICATION->ShowTitle()?>">

    </div>
    <?=$arResult["FORM_FOOTER"]?>
<?php endif; ?>
</div>
    <div class="zufar" style="display: none;">
    <? print_r( $arResult["QUESTIONS"] )?>
    </div>
</div>
</div>