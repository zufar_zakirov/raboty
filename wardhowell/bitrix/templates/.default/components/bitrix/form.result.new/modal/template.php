<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>
<button type="button" data-fancybox-close class="fancybox-button fancybox-close-small" title="Закрыть">
<svg class="svg svg-arow-right" width="30" height="30"><use xlink:href="/ico/sprite/sprite.svg#close"></use></svg>
</button>
<div class="resume container">
<div class="resume__wrap">
<h2><?=$arResult["FORM_TITLE"]?></h2>
<? if (!empty($arResult['FORM_DESCRIPTION'])) : ?>
<p><?= $arResult['FORM_DESCRIPTION'] ?></p>
<? endif; ?>
<?php 
if ($arResult["isFormNote"] == "Y"): ?>
    <div class="callout success">Спасибо. Ваше сообщение успешно отправлено.</div>
<?php else: ?>
	<?=$arResult["FORM_HEADER"]?>
	<?
	foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion) {
		if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'hidden') {
			print $arQuestion["HTML_CODE"];
		}
	}
	?>
	<?if ($arResult["isFormErrors"] == "Y"):?>
        <?=$arResult["FORM_ERRORS_TEXT"]?>
    <?endif;?>
    <div class="resume__form">
        <?

        foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion) : 
            $label = $arQuestion["CAPTION"].($arQuestion["REQUIRED"] == "Y" ? ' *' : '');
            $isError = !(empty($arResult['FORM_ERRORS_TEXT']) || false === strpos($arResult['FORM_ERRORS_TEXT'], $arQuestion["CAPTION"]));
        ?>
            <? if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'text' || $arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'email'): ?>
                <?
					print str_replace('class="inputtext"', 'class="resume__form-input resume__form-input--50" placeholder="'.$label.'"', $arQuestion["HTML_CODE"]); 
                ?>
            <? elseif ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'textarea') : ?>
				<script>
				function auto_grow(element)
				{
					element.style.height = "10px";
					element.style.height = (element.scrollHeight)+"px";
                }
                </script>
                <?
					print str_replace('class="inputtextarea"', ' class="resume__form-textarea" onkeyup="auto_grow(this)" placeholder="'.$label.'"', $arQuestion["HTML_CODE"]);
                ?>
			<? elseif ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'file') : ?>
				<div class="input-file" data-input-file="">
                <?
					print $arQuestion["HTML_CODE"];
                ?>
					<svg class="svg svg-attachment" width="50" height="50"><use xlink:href="/ico/sprite/sprite.svg#attachment"></use></svg> 
					<span data-input-file-selected=""><?= $arQuestion["CAPTION"] ?></span>
                </div>
            <? elseif ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'checkbox') : ?>
				<div class="block-check">
					<?
                        print str_replace('<label for', '<label for="13" class="block-check__box"></label><label class="block-check__text " for', $arQuestion["HTML_CODE"]); 
					?>
                </div>
            <? endif; ?>
        <?endforeach;?>
		
        <div class="resume__form-btn">
			<button type="submit" class="btn" name="web_form_submit" value="<?=htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]);?>"><span><?=htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]);?></span></button>
        </div>
    </div>
    <?=$arResult["FORM_FOOTER"]?>
<?php endif; ?>
</div>
</div>