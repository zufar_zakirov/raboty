<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>
<button type="button" data-fancybox-close class="fancybox-button fancybox-close-small" title="Закрыть">
<svg class="svg svg-arow-right" width="30" height="30"><use xlink:href="/ico/sprite/sprite.svg#close"></use></svg>
</button>
<div class="resume container">
<div class="resume__wrap">
<h2><?=$arResult["FORM_TITLE"]?></h2>
<? if (!empty($arResult['FORM_DESCRIPTION'])) : ?>
<p><?= $arResult['FORM_DESCRIPTION'] ?></p>
<? endif; ?>
<?php 
if ($arResult["isFormNote"] == "Y"): ?>
    <div class="callout success">Thanks for the confirmation</div>
<?php else: ?>
	<?=$arResult["FORM_HEADER"]?>
	<?
	foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion) {
		if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'hidden') {
			print $arQuestion["HTML_CODE"];
		}
	}
	?>	
    <div class="resume__form resume__form--policy">
        <?
        foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion) : 
            $label = $arQuestion["CAPTION"].($arQuestion["REQUIRED"] == "Y" ? ' *' : '');
            $isError = !(empty($arResult['FORM_ERRORS_TEXT']) || false === strpos($arResult['FORM_ERRORS_TEXT'], $arQuestion["CAPTION"]));
        ?>
            <? if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'text' || $arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'email'): ?>
                <?
					print str_replace('class="inputtext"', 'class="resume__form-input resume__form-input--50" placeholder="'.$label.'"', $arQuestion["HTML_CODE"]); 
                ?>
            <? elseif ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'textarea') : ?>
				<script>
				function auto_grow(element)
				{
					element.style.height = "10px";
					element.style.height = (element.scrollHeight)+"px";
                }
                </script>


                <?
					print str_replace('class="inputtextarea"', ' class="resume__form-textarea policy" onkeyup="auto_grow(this)" placeholder="'.$label.'"', $arQuestion["HTML_CODE"]);
                ?>

			<? elseif ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'file') : ?>
				<div class="input-file" data-input-file="">
                <?
					print $arQuestion["HTML_CODE"];
                ?>
					<svg class="svg svg-attachment" width="50" height="50"><use xlink:href="/ico/sprite/sprite.svg#attachment"></use></svg> 
					<span data-input-file-selected=""><?= $arQuestion["CAPTION"] ?></span>
                </div>
	<? elseif ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'dropdown') : ?>

of my own free will and volition express my informed consent to process my Personal Data by JSC “VECTOR LEADERSTVA” at the address: 121151, Moscow, Mozhaisky Val street, 8 bldg. C, floor 12 space  LXXXVIII room 7, office 12.20 and its affiliated companies for purposes:
			<?
				print str_replace('class="inputtextarea"', ' class="resume__form-textarea policy" onkeyup="auto_grow(this)" placeholder="'.$label.'"', $arQuestion["HTML_CODE"]);
                		?>		


            <? elseif ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'checkbox') : ?>
				<div class="block-check__label"><?= $label ?></div>
				<div class="block-check">
					<?
                        $output = str_replace('<label for', '<label for="CC_ID" class="block-check__box"></label><label class="block-check__text" for', $arQuestion["HTML_CODE"]); 

						foreach ($arQuestion['STRUCTURE'] as $question) {
							$output = preg_replace('/CC_ID/', $question['ID'], $output, 1);
						}
						
						print str_replace('<br />', '</div><div class="block-check">', $output);
					?>
                </div>
				<? if (! empty($arResult['arQuestions'][$FIELD_SID]['COMMENTS'])) : ?>
					<div class="block-check__help"><?= $arResult['arQuestions'][$FIELD_SID]['COMMENTS'] ?></div>
				<? endif; ?>
            <? endif; ?>
        <?endforeach;?>


        <?if ($arResult["isFormErrors"] == "Y"):?>
        		<div>
        		<?=$arResult["FORM_ERRORS_TEXT"]?>
        		</div>
        		<!-- <br /><br /> -->
    	<?endif;?>
<div> 
<!-- <p>
of my own free will and volition express my informed consent to process my Personal Data by JSC “VECTOR LEADERSTVA” at the address: 121151, Moscow, Mozhaisky Val street, 8 bldg. C, floor 12 space  LXXXVIII room 7, office 12.20 and its affiliated companies for purposes: of informational and analitycal direct mail   This includes collection, recording, systematization, accumulation, storage, clarification (update, correction), retrieval, usage, transfer (distribution, provision, access), depersonalization, blocking, removal, destruction, transboundary transfer of Personal Data (including any countries without adequate protection of the personal data), as well as for execution of any other actions in respect to such Personal Data in accordance with current legislation of the Russian Federation, executed by or without means of automated processing.
</p>
<p>
The list of Personal Data subject to processing includes name, last name, second name, date and place of birth, address of place of residence and address of registration, marital, social and property status, education, qualification and other skills, profession, information on previous places of work, functions, expected level of remuneration, names of referees, hobby, personal characteristics, and any other information concerning my personality accessible or already known by JSC “VECTOR LEADERSTVA”.
</p>
<p>
I hereby acknowledge and accept that in case it is necessary for the above-stated purposes to grant my Personal Data to the third party, JSC “VECTOR LEADERSTVA” is entitled to disclose my Personal Data to such third parties and other authorized persons, only to the extend required for the above-stated actions, as well as to provide such third parties with the relevant documents containing my Personal Data.
</p>
<p>
This Consent to process Personal Data is executed for indefinite period and is valid until the moment of withdrawal of Consent. The Consent can be withdrawn by sending of correspondent written notice to JSC “VECTOR LEADERSTVA” at the address: 121151, Moscow, Mozhaisky Val street, 8 bldg. C, floor 12 space  LXXXVIII room 7, office 12.20. In case of withdraw of consent receipt  JSC “VECTOR LEADERSTVA” is obliged to dismiss the processing of Personal Data, and in case the saving of the such Data is not required for the purposes of Personal Data’ processing - to delete them not later than after 3 days from the date of withdraw receipt.     
</p>
<p>
An order of Consent for Personal Data processing withdrawal is agreed.
</p> -->
</div>
    	
		<div style="clear: both;"><hr /></div>
        <div class="resume__form-btn">
			<button type="submit" class="btn" name="web_form_submit" value="<?=htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]);?>"><span><?=htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]);?></span></button>
        </div>
    </div>
    <?=$arResult["FORM_FOOTER"]?>
<?php endif; ?>
</div>
</div>