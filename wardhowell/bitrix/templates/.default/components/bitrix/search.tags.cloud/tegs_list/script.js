$(function () {
    var tags = $('.js-tei-tags a'),
        tags_timeout = 0;

    if (! tags.length) {
        return;
    }

    tags.on('click', function (e) {
        e.preventDefault();

        if (tags_timeout)
            clearTimeout(tags_timeout);
		
		var tag_url = this.href;
		
		tags.removeClass('active');
		$(this).addClass('active');

        tags_timeout = setTimeout(function () {
            $.get(tag_url, function (response) {
                $('.js-tei-articles').html($(response).find('.js-tei-articles').html());
				
				$('html, body').animate({
					scrollTop: $('.tei-label').offset().top - 70
				}, 750);
            });
        }, 1000);
    });
});