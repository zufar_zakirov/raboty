(function($) {
    var pagen = 0;
    var totalPages = 0;
	
    $(function() {
        $('body').on('click', '.js-is-more[data-total]', function(e) {
			if ($('.js-is-container').not('.js-is-init')) {
				e.preventDefault();
				
				$('.js-is-container').addClass('js-is-init');
				
				pagen = parseInt($(this).attr('data-pagen'));
				totalPages = parseInt($(this).attr('data-total'));

				var infScroll = new InfiniteScroll( '.js-is-container', {
					path: getNextPath,
					checkLastPage: true,
					append: '.js-is-container-item',
					status: '.c-page-load-status',
					history: false,
					prefill: false,
					button: '.js-is-more',
					scrollThreshold: false,
				});
				
				infScroll.loadNextPage();
			}
		});
    });
    function getNextPath() {
        var nextPage = this.loadCount + 2;

        if (totalPages >= nextPage) {
            return window.location.pathname+'?PAGEN_'+pagen+'='+nextPage+'&load_more'
        }
    }
})(jQuery);