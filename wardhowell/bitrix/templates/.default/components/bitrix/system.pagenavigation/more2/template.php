<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->setFrameMode(true);

if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false)) {
	return;
}

$nextUrl = $APPLICATION->GetCurPageParam('PAGEN_' . $arResult['NavNum'] . '='.($arResult["NavPageNomer"]+1), ['PAGEN_' . $arResult['NavNum'], 'load-more'])
?>
<? if ($arResult['NavPageNomer'] < $arResult['NavPageCount']): ?>
<div class="c-page-load-status">
	<div class="infinite-scroll-request"></div>
</div>
<div class="article-search__more js-is-more" data-pagen="<?= $arResult['NavNum']; ?>" data-total="<?= $arResult['NavPageCount']; ?>">
	<a href="<?= $nextUrl; ?>" class="btn"><span><?=GetMessage("LOOK_MORE")?></span></a>
</div>
<? endif;?>