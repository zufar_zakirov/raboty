<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?>
<div class="header__wrap">
	<div class="container">
		<div class="header__link">
			<?foreach($arResult as $arItem):?>
				<a href="<?=$arItem["LINK"]?>" class="header__link-item"><span><?=$arItem["TEXT"]?></span></a>				
			<?endforeach?>
		</div>
	</div>
</div>	
<?endif?>