<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="article-search">
<div class="container">
<div class="article-search__grid">

<?
// $APPLICATION->IncludeComponent("bitrix:catalog.section.list", "tree", array(
// 	"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
// 	"IBLOCK_ID" => $arParams["IBLOCK_ID"],
// 	"SECTION_ID" => "0",
// 	"COUNT_ELEMENTS" => "Y",
// 	"TOP_DEPTH" => "2",
// 	"SECTION_URL" => $arParams["SECTION_URL"],
// 	"CACHE_TYPE" => $arParams["CACHE_TYPE"],
// 	"CACHE_TIME" => $arParams["CACHE_TIME"],
// 	"DISPLAY_PANEL" => "N",
// 	"ADD_SECTIONS_CHAIN" => $arParams['ADD_SECTIONS_CHAIN'],
// 	"SECTION_USER_FIELDS"	=>	$arParams["SECTION_USER_FIELDS"],
// 	),
// 	$component
// );
?>


		<?foreach($arResult["ITEMS"] as $cell=>$arElement):?>

		<?
		$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCT_ELEMENT_DELETE_CONFIRM')));
		 //= $arElement["DISPLAY_PROPERTIES"]["URL"]["VALUE"];
		?>

		<div class="consitem consitem--news">
                        	<i class="consitem__avatar">                       	
		
		<a href="<?=$arElement["DETAIL_PAGE_URL"]?>">	
		<img border="0"
			src="<?=$arElement["DISPLAY_PROPERTIES"]["IMAGE_COVER"]["FILE_VALUE"]["SRC"]?>"
			/></a>
			</i>

			
                            <div class="consitem__info">
                            <a href="<?=$arElement["DETAIL_PAGE_URL"]?>">
                            <h4><?=$arElement["NAME"]?></h4></a>
                                <p><?=$arElement["PREVIEW_TEXT"]?></p>
                                <a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><i><svg class="svg svg-arrow" width="50" height="50"><use xlink:href="ico/sprite/sprite.svg#arrow"></use></svg></i></a>
                            </div>
	             </div>	

		<?endforeach; // foreach($arResult["ITEMS"] as $arElement):?>
		


</div>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
</div>
</div>
