<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$disp_prop = $arResult["DISPLAY_PROPERTIES"];

$current_url = 'http://'.$_SERVER['HTTP_HOST'] . $APPLICATION->GetCurPage(true);
$summary = $disp_prop['TEXT_1']['~VALUE']['TEXT'];
$image = $disp_prop['IMAGE_ARTICLE']['FILE_VALUE']['SRC'];


if ($arResult["FIELDS"]["TAGS"] !=''){
	$tags_arr = explode(',', $arResult["FIELDS"]["TAGS"]);
}
?>

<?
// Создаем изображение для репостов соц сетей.  OpenGraf
$meta_og_img = CFile::ResizeImageGet( $arResult["DISPLAY_PROPERTIES"]["IMAGE_COVER"]["FILE_VALUE"]["ID"]
        , Array("width" => 600, "height" => 400), BX_RESIZE_IMAGE_EXACT, false);

$APPLICATION->SetPageProperty("meta_og_img",  $meta_og_img['src']);
?>


<div class="material__header jarallax" style="background-image: url(<?=$arResult["DISPLAY_PROPERTIES"]["IMAGE_COVER"]["FILE_VALUE"]["SRC"]?>)">

<div class="container">
                    <div class="material__header-content">
                        <div>
                            <time><?=$arResult["DISPLAY_ACTIVE_FROM"]?></time>
                        </div>
                        <h1><?=$arResult["NAME"]?></h1>
                        <div class="label">
                        <?php foreach ($tags_arr as $key => $value): ?>
                        	<a href="<?=SITE_DIR?>tags/?tag=<?=$value?>"><?=$value?></a>
                        <?php endforeach ?>
                        </div>
                    </div>
</div>
</div>

          <div class="material__wrap">
                <div class="container">
                    <div class="block-page__grid">
                        <div class="block-page__content">
                            <div class="panel-article">
                                <div class="panel-article__social">
			<a href="javascript: void(0)" data-sharer="facebook" data-url="<?=$current_url?>">
                                        <svg class="svg svg-facebook" width="50" height="50">
                                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/ico/sprite/sprite.svg#facebook"></use>
                                        </svg>
                                    </a>
                                    <a href="#in" data-sharer="linkedin" data-url="<?=$current_url?>">
                                        <svg class="svg svg-linkedin" width="50" height="50">
                                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/ico/sprite/sprite.svg#linkedin"></use>
                                        </svg>
                                    </a>
									<span><? echo GetMessage('SHARE'); ?></span>
                                </div>
                                <div class="panel-article__btn journal__footer">
                                    <a href="#envelope" class="journal__envelope js-share-email">
                                        <svg class="svg svg-envelope" width="50" height="50">
                                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/ico/sprite/sprite.svg#envelope"></use>
                                        </svg> <span><? echo GetMessage('SEND_EMAIL'); ?></span> 
                                    </a>
                                    <a href="#print" class="journal__printer js-print">
                                        <svg class="svg svg-printer" width="50" height="50">
                                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/ico/sprite/sprite.svg#printer"></use>
                                        </svg> <span><? echo GetMessage('PRINT'); ?></span> 
                                    </a>
                                    <a href="#pdf" class="journal__pdf">
                                        <svg class="svg svg-pdf" width="50" height="50">
                                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/ico/sprite/sprite.svg#pdf"></use>
                                        </svg> <span><? echo GetMessage('SAVE'); ?></span>
                                    </a>
                                </div>
                            </div>
                            <article>

<?php
    if ($arResult['YOUTUBE']):
?>
<div class="video">
<iframe width="640" height="360" src="https://www.youtube.com/embed/<?=$arResult['YOUTUBE']?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
<?php
    endif;
?>

<div class="cont_tei">
<?=$arResult['~TEXT_1'] ?>
</div>

<?php if (24 == $arResult['IBLOCK_SECTION_ID']): ?>

<div class="article-address">
            <div class="article-address__item"><b><? echo GetMessage('DATE_EVENT'); ?></b>  
                <span>
                    <?                                        
                    if ($TS_stamp = MakeTimeStamp($disp_prop['TIME_START']['VALUE'])) 
                    {                                         
                        echo date("d.m.Y (H:i - ", $TS_stamp); 
                    } 
                    if ($TS_stamp = MakeTimeStamp($disp_prop['TIME_END']['VALUE'])) 
                    {                                         
                        echo date(" H:i)", $TS_stamp); 
                    }

                    ?>

                </span>
                </div>
                <div class="article-address__item"><b><? echo GetMessage('LOCATION'); ?></b>  
                <span>
                   <?=$disp_prop['ADDRESS']['VALUE'];?>                                    
                </span>
             </div>
</div>
<?php endif; ?>                                
                                	
                                </p>
                            </article>
                        </div>
                        <div class="block-page__aside">
<? if( $disp_prop['AUTHORS'] !== NULL OR $disp_prop['AUTHORS_2'] !== NULL ) : ?>
    <h3><? echo GetMessage('AUTHORS'); ?></h3>                            
<? endif; ?>    
                            <div class="block-page__author">

<? if($disp_prop['AUTHORS'] !== NULL): ?>
                            

                            <? $k = 0;  ?>
                            <? foreach ($disp_prop['AUTHORS']['LINK_ELEMENT_VALUE'] as $key => $value):?>

                            <div class="block-page__author-item">

<?php
$id_author = $disp_prop['AUTHORS']['VALUE'][$k];
$id_iblock_author = $disp_prop['AUTHORS']["LINK_IBLOCK_ID"];

$arFilter = Array("IBLOCK_ID"=>$id_iblock_author, "ID"=>$id_author);
$res = CIBlockElement::GetList(Array(), $arFilter);
if ($ob = $res->GetNextElement()){;
    $arFields = $ob->GetFields(); // поля элемента    
    $arProps = $ob->GetProperties(); // свойства элемента    
}             
?>

                                <i><img src="<?=CFile::GetPath($value["PREVIEW_PICTURE"])?>" alt="IMG"></i>
                                    <div><b><?=$value['NAME']?></b>  
                                    <span>
                                    <?=$arProps['JOB_POSITION']['~VALUE']?>                                    
                                    </span>
                                    </div>
                                </div>
                            <? $k++; endforeach; ?>
                            
<? endif; ?>


<? if($disp_prop['AUTHORS_2'] !== NULL): ?>                                                        
                            <? $i = 0; ?>
                            <? foreach ($disp_prop['AUTHORS_2']['LINK_ELEMENT_VALUE'] as $key => $value):?>                            
<?php

$id_author = $disp_prop['AUTHORS_2']['VALUE'][$i];
$id_iblock_author = $disp_prop['AUTHORS_2']["LINK_IBLOCK_ID"];

$arFilter2 = Array("IBLOCK_ID"=>$id_iblock_author, "ID"=>$id_author);
$res = CIBlockElement::GetList(Array(), $arFilter2);
if ($ob = $res->GetNextElement() ){
    
    $arFields = $ob->GetFields(); // поля элемента    
    $arProps2 = $ob->GetProperties(); // свойства элемента    
}

$renderImage = CFile::ResizeImageGet($value["PREVIEW_PICTURE"], Array("width" => 101, "height" => 101), BX_RESIZE_IMAGE_EXACT, false); 

?>
                            <div class="block-page__author-item block">
                                <img src="<?=$renderImage['src']?>" align="left">
                                <p><b><?=$value['NAME']?></b><br />
                                    <?=$arProps2['JOB_POSITION']['~VALUE']?>                                    
                                    </p>                                    
                            </div>
                            <? $i++; endforeach; ?>                            
<? endif; ?>
</div>



                            <h3><? echo GetMessage('MOST_POPULAR'); ?></h3>

<div class="block-page__popular">

<?
$IBLOCK_ID = 1;
$IBLOCK_TYPE = 'news';

if (SITE_ID == 's2'){
    $IBLOCK_ID = 18;
    $IBLOCK_TYPE = 'catalog_eng';

}

$APPLICATION->IncludeComponent("bitrix:news.list", "event", Array(
	"ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
		"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
		"AJAX_MODE" => "Y",	// Включить режим AJAX
		"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
		"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
		"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
		"AJAX_OPTION_STYLE" => "N",	// Включить подгрузку стилей
		"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
		"CACHE_GROUPS" => "Y",	// Учитывать права доступа
		"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
		"CACHE_TYPE" => "A",	// Тип кеширования
		"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
		"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
		"DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
		"DISPLAY_DATE" => "Y",	// Выводить дату элемента
		"DISPLAY_NAME" => "Y",	// Выводить название элемента
		"DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
		"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
		"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
		"FIELD_CODE" => array(	// Поля
			0 => "NAME",
			1 => "TAGS",
			2 => "PREVIEW_TEXT",
			3 => "PREVIEW_PICTURE",
			4 => "",
		),
		"FILTER_NAME" => "",	// Фильтр
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
		"IBLOCK_ID" => $IBLOCK_ID,	// Код информационного блока
		"IBLOCK_TYPE" => $IBLOCK_TYPE,	// Тип информационного блока (используется только для проверки)
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
		"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
		"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
		"NEWS_COUNT" => "5",	// Количество новостей на странице
		"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
		"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
		"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
		"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
		"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
		"PAGER_TITLE" => "Новости",	// Название категорий
		"PARENT_SECTION" => "",	// ID раздела
		"PARENT_SECTION_CODE" => "",	// Код раздела
		"PREVIEW_TRUNCATE_LEN" => "130",	// Максимальная длина анонса для вывода (только для типа текст)
		"PROPERTY_CODE" => array(	// Свойства
			0 => "TYPE",
			1 => "VIDEO",
			2 => "",
		),
		"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
		"SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
		"SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
		"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
		"SET_STATUS_404" => "N",	// Устанавливать статус 404
		"SET_TITLE" => "N",	// Устанавливать заголовок страницы
		"SHOW_404" => "N",	// Показ специальной страницы
		"SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
		"SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
		"SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
		"SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
		"STRICT_SECTION_CHECK" => "N",	// Строгая проверка раздела для показа списка
		"COMPONENT_TEMPLATE" => "tei_item_only_header"
	),
	false
);?>

                            </div>
<?  $journal_id = $disp_prop['JOURNAL']['VALUE']; ?>
<?if($journal_id >0): ?>
                            <h3><? echo GetMessage('ISSUES'); ?></h3>
                            <div class="block-page__journal">
                                <div class="journal">
                                    <div class="journal__ico">
                                    <a href="<?=$disp_prop['JOURNAL']['LINK_ELEMENT_VALUE'][$journal_id]['DETAIL_PAGE_URL']?>"><i>
                                    <img src="<?=CFile::GetPath( $disp_prop['JOURNAL']['LINK_ELEMENT_VALUE'][$journal_id]['PREVIEW_PICTURE'] )?>" alt="IMG"></i>
                                    </a>
                                    </div>                                    
                                    <div class="journal__footer">
                                    <a href="<?=$disp_prop['JOURNAL']['LINK_ELEMENT_VALUE'][$journal_id]['DETAIL_PAGE_URL']?>" class="journal__download"><? echo GetMessage('READ_JOURNAL_ONLINE'); ?></a> 
                                        <a href="#pdf" class="journal__pdf">
                                            <svg class="svg svg-pdf" width="50" height="50">
                                                <use xlink:href="<?=SITE_TEMPLATE_PATH?>/ico/sprite/sprite.svg#pdf"></use>
                                            </svg>
                                        </a>
                                        <a href="#print" class="journal__printer">
                                            <svg class="svg svg-printer" width="50" height="50">
                                                <use xlink:href="<?=SITE_TEMPLATE_PATH?>/ico/sprite/sprite.svg#printer"></use>
                                            </svg>
                                        </a>
                                        <a href="#envelope" class="journal__envelope">
                                            <svg class="svg svg-envelope" width="50" height="50">
                                                <use xlink:href="<?=SITE_TEMPLATE_PATH?>/ico/sprite/sprite.svg#envelope"></use>
                                            </svg>
                                        </a>
                                    </div>
                                </div>
                            </div><a href="<?=SITE_DIR?>journal/ " class="btn"><span><? echo GetMessage('ALL_ISSUES'); ?></span></a>
<?endif;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

</div>




<?php if (24 == $arResult['IBLOCK_SECTION_ID']): ?>

<?php endif; ?>