<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

//IncludeTemplateLangFile('/bitrix/templates/.default/lang/lang.php');
IncludeTemplateLangFile(__FILE__);


$disp_prop = $arResult["DISPLAY_PROPERTIES"];

if ($arResult["FIELDS"]["TAGS"] !=''){
	$tags_arr = explode(',', $arResult["FIELDS"]["TAGS"]);
}
?>

<div class="block-page__content">
                        <article>
                            <h2><?=$arResult["NAME"]?></h2>
  <?=$disp_prop['ABOUT_SEVICE']['~VALUE']['TEXT']?>

<? if( count($disp_prop['HEADER_FAQ']['VALUE']) ): ?>
<dl>
<? foreach ($disp_prop['HEADER_FAQ']['VALUE'] as $key => $value): ?>
    <dt><span><?=$value?></span></dt>
    <dd>    
        <?=$disp_prop['TEXT_FAQ']['~VALUE'][$key]['TEXT']?>    
    </dd>    
<? endforeach; ?>
</dl>
<? endif; ?>

</article>
<? if ( $arResult['ID'] == 229 or $arResult['ID'] == 267 ): ?>
<?
if ($arResult['ID'] == 229){
    $IBLOCK_ID = "6";
    $IBLOCK_TYPE = "Catalog";
}
elseif( $arResult['ID'] == 267 ){
    $IBLOCK_ID = '15';
    $IBLOCK_TYPE = 'catalog_eng';
}
?>

<?$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "number_of_company_2",
    Array(
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "ADD_SECTIONS_CHAIN" => "N",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "N",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "CHECK_DATES" => "Y",
        "COMPONENT_TEMPLATE" => "number_of_company_2",
        "DETAIL_URL" => "",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "DISPLAY_TOP_PAGER" => "N",
        "FIELD_CODE" => array(0=>"NAME",1=>"DETAIL_TEXT",2=>"",),
        "FILTER_NAME" => "",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "IBLOCK_ID" => $IBLOCK_ID ,
        "IBLOCK_TYPE" => $IBLOCK_TYPE,
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "INCLUDE_SUBSECTIONS" => "Y",
        "MESSAGE_404" => "",
        "NEWS_COUNT" => "3",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => ".default",
        "PAGER_TITLE" => "Новости",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "PROPERTY_CODE" => array(0=>"",1=>"",),
        "SET_BROWSER_TITLE" => "N",
        "SET_LAST_MODIFIED" => "N",
        "SET_META_DESCRIPTION" => "N",
        "SET_META_KEYWORDS" => "N",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "N",
        "SHOW_404" => "N",
        "SORT_BY1" => "ACTIVE_FROM",
        "SORT_BY2" => "SORT",
        "SORT_ORDER1" => "DESC",
        "SORT_ORDER2" => "ASC",
        "STRICT_SECTION_CHECK" => "N"
    )
);?>
<? endif; ?>
</div>


<div class="block-page__aside">
<?php
if ( count ($disp_prop['FIRST_CONSULTANT']) > 0  ||  count ($disp_prop['CONSULTANTS']) >0 ):
?>
<h3><? echo GetMessage( 'CONSULTANTS' ); ?></h3>
<? endif; ?>
<div class="block-page__author">
<?
// FIRST_CONSULTANT
 if ($disp_prop['FIRST_CONSULTANT'] >0):?>
            
            
<? $i = 0;
 foreach ($disp_prop['FIRST_CONSULTANT']['LINK_ELEMENT_VALUE'] as $key => $value): ?>
<?
$id_author = $disp_prop['FIRST_CONSULTANT']['VALUE'][$i];
$id_iblock_author = $disp_prop['FIRST_CONSULTANT']["LINK_IBLOCK_ID"];

$arFilter = Array("IBLOCK_ID"=>$id_iblock_author, "ID"=>$id_author);
$res = CIBlockElement::GetList(Array(), $arFilter);
if ($ob = $res->GetNextElement()){;
    $arFields = $ob->GetFields(); // поля элемента    
    $arProps = $ob->GetProperties(); // свойства элемента    
}
?>
                    <div class="block-page__author-item block">
                        <a href="<?=$value['DETAIL_PAGE_URL']?>">
                            <img src="<?=CFile::GetPath($value['PREVIEW_PICTURE'])?>" align="left">                            
                            <p><b><?=$value['NAME']?></b><br  />
                            <?=$arProps['JOB_POSITION']['~VALUE']?></p>
                        </a>
                    </div>
            <? $i ++;
             endforeach; ?>            

<? endif; 
// FIRST_CONSULTANT
?>


<? if ($disp_prop['CONSULTANTS'] > 0 ):?>            
            
<?  $i = 0;
 foreach ($disp_prop['CONSULTANTS']['LINK_ELEMENT_VALUE'] as $key => $value): ?>
<?
$id_author = $disp_prop['CONSULTANTS']['VALUE'][$i];
$id_iblock_author = $disp_prop['CONSULTANTS']["LINK_IBLOCK_ID"];

$arFilter = Array("IBLOCK_ID"=>$id_iblock_author, "ID"=>$id_author);
$res = CIBlockElement::GetList(Array(), $arFilter);
if ($ob = $res->GetNextElement()){;
    $arFields = $ob->GetFields(); // поля элемента    
    $arProps = $ob->GetProperties(); // свойства элемента    
}

$renderImage = CFile::ResizeImageGet($value["PREVIEW_PICTURE"], Array("width" => 101, "height" => 101), BX_RESIZE_IMAGE_EXACT, false); 

?>
                    <div class="block-page__author-item block" >
                            <a href="<?=$value['DETAIL_PAGE_URL']?>">                        
                                    <img src="<?=$renderImage['src']?>"  align="left">                        
                                   <p><b><?=$value['NAME']?></b><br/>
                                   <?=$arProps['JOB_POSITION']['~VALUE']?></p>
                            </a>
                    </div>
<?
$i++;
 endforeach; ?>
            
            <a href="<?=SITE_DIR?>consultants/" class="btn"><span><? echo GetMessage( 'SEARCH_CONSULTANTS' ); ?></span></a>
<? endif; ?>


<? if ( $arResult['ID'] == 76 OR $arResult['ID'] == 182 ): ?>
<?
$dr_r_id = "31";
if ( 's2' == SITE_ID ){
    $dr_r_id = "35";
}

//$GLOBALS['arrFilter'] = array("PROPERTY_FIRST_INDUSTRY" =>  $industry_id  );
$first_consult = $APPLICATION->IncludeComponent(
    "bitrix:news.list", 
    "consultants_vertical_side_no_link", 
    array(
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "ADD_SECTIONS_CHAIN" => "N",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "N",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "DISPLAY_TOP_PAGER" => "N",
        "FIELD_CODE" => array(
            0 => "",
            1 => "",
        ),
        "FILTER_NAME" => "",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "IBLOCK_ID" => $dr_r_id,
        "IBLOCK_TYPE" => "-",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "INCLUDE_SUBSECTIONS" => "Y",
        "MESSAGE_404" => "",
        "NEWS_COUNT" => "18",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => ".default",
        "PAGER_TITLE" => "Консультанты",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "PROPERTY_CODE" => array(
            0 => "JOB_POSITION",
            1 => "SPECIALIZATION",
            2 => "",
        ),
        "SET_BROWSER_TITLE" => "N",
        "SET_LAST_MODIFIED" => "N",
        "SET_META_DESCRIPTION" => "N",
        "SET_META_KEYWORDS" => "N",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "N",
        "SHOW_404" => "N",
        "SORT_BY1" => "SORT",
        "SORT_BY2" => "SORT",
        "SORT_ORDER1" => "ASC",
        "SORT_ORDER2" => "ASC",
        "STRICT_SECTION_CHECK" => "N",
        "COMPONENT_TEMPLATE" => "consultants_vertical_side"
    ),
    false
);?>
<? endif; ?>


</div>
</div>