<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

//IncludeTemplateLangFile('/bitrix/templates/.default/lang/lang.php');
IncludeTemplateLangFile(__FILE__);


$disp_prop = $arResult["DISPLAY_PROPERTIES"];

if ($arResult["FIELDS"]["TAGS"] !=''){
	$tags_arr = explode(',', $arResult["FIELDS"]["TAGS"]);
}
?>

<div class="block-page__content">
                        <article>
                            <h2><?=$arResult["NAME"]?></h2>
  <?=$disp_prop['ABOUT_SEVICE']['~VALUE']['TEXT']?>
<dl>
<? foreach ($disp_prop['HEADER_FAQ']['VALUE'] as $key => $value): ?>
    <dt><span><?=$value?></span></dt>
    <dd>
    <p>
        <?=$disp_prop['TEXT_FAQ']['~VALUE'][$key]['TEXT']?>
    </p>
    </dd>    
<? endforeach; ?>
</dl>
</article>
</div>
<? if ($disp_prop['CONSULTANTS'] !== NULL):?>
<div class="block-page__aside">
            <h3><? echo GetMessage( 'CONSULTANTS' ); ?></h3>
            <div class="block-page__slider">
<?  foreach ($disp_prop['CONSULTANTS']['LINK_ELEMENT_VALUE'] as $key => $value): ?>
<?
$id_author = $disp_prop['CONSULTANTS']['VALUE'][0];
$id_iblock_author = $disp_prop['CONSULTANTS']["LINK_IBLOCK_ID"];

$arFilter = Array("IBLOCK_ID"=>$id_iblock_author, "ID"=>$id_author);
$res = CIBlockElement::GetList(Array(), $arFilter);
if ($ob = $res->GetNextElement()){;
    $arFields = $ob->GetFields(); // поля элемента    
    $arProps = $ob->GetProperties(); // свойства элемента    
}
?>
                    <a class="consitem" href="<?=$value['DETAIL_PAGE_URL']?>">
                        <i class="consitem__avatar">
                            <img src="<?=CFile::GetPath($value['PREVIEW_PICTURE'])?>" alt="IMG"></i>
                             <div class="consitem__info">
                                        <h4><?=$value['NAME']?></h4>                                        
                                        <p><?=$arProps['JOB_POSITION']['~VALUE']?></p>
                             </div>
                    </a>
            <? endforeach; ?>
            </div>
            <a href="<?=SITE_DIR?>consultants/" class="btn"><span><? echo GetMessage( 'SEARCH_CONSULTANTS' ); ?></span></a>
</div>
<? endif; ?>