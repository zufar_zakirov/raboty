<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$disp_prop = $arResult["DISPLAY_PROPERTIES"];

?>
<div class="stage">
    <div class="container">
        <div class="stage__wrap">
            <div class="stage__header">
                <h2><?=$arResult["NAME"]?></h2>
                <p><?=$arResult["DISPLAY_PROPERTIES"]["TEXT"]["VALUE"]['TEXT']?></p>
            </div>
            <ul>
                <li><b>1</b>  <span>Клиент</span>
                    <p>
                    <?=$arResult["DISPLAY_PROPERTIES"]["TEXT_1"]["VALUE"]['TEXT']?>
                    </p>
                </li>
                <li><b>2</b>  <span>Профессионализм</span>
                    <p>
                    <?=$arResult["DISPLAY_PROPERTIES"]["TEXT_2"]["VALUE"]['TEXT']?>
                    </p>
                </li>
                <li><b>3</b>  <span>Успех</span>
                    <p>
                        <?=$arResult["DISPLAY_PROPERTIES"]["TEXT_3"]["VALUE"]['TEXT']?>
                    </p>
                </li>
                <li><b>4</b>  <span>Команда</span>
                    <p>
                    <?=$arResult["DISPLAY_PROPERTIES"]["TEXT_4"]["VALUE"]['TEXT']?>
                    </p>
                </li>
            </ul>
        </div>
    </div>
</div>