<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$galleryHtml = '';
$imageFieldName = 'IMAGE_ARTICLE';
$text_1 = $arResult["DISPLAY_PROPERTIES"]['TEXT_1']['~VALUE']['TEXT'];

if (! empty($arResult["DISPLAY_PROPERTIES"][$imageFieldName]['FILE_VALUE'])) {

	$galleryImageField = &$arResult["DISPLAY_PROPERTIES"][$imageFieldName];
	
	$galleryHtml .= '<div class="article-slider"><div class="article-slider__max js__slide-article-max">';
	
	foreach ($galleryImageField['FILE_VALUE'] as $key => $value) {

$renderImage = CFile::ResizeImageGet( $value["ID"]
        , Array("width" => 770, "height" => 593), BX_RESIZE_IMAGE_EXACT, false);

		$galleryHtml .= '<div><img src="'.$renderImage['src'].'"  /></div>';
	}
	$galleryHtml .= '</div><div class="article-slider__min js__slide-article-min">';

	foreach ($galleryImageField['FILE_VALUE'] as $key => $value) {
		$galleryHtml .= '<div class="article-slider__min-item"><img src="'.$value['SRC'].'" alt="" /></div>';
	}
	
	$galleryHtml .= '</div></div>';
}

$arResult['~TEXT_1'] = str_replace('#GALLERY#', $galleryHtml, $text_1 ) ;





function youtube_code($url)
{
	$youtu_code = FALSE;

	if( strpos($url, 'youtube.com') )
	{		
		preg_match ( '/watch\?v=([A-z0-9]*)/' , $url , $matches);
		$youtu_code = $matches[1];
	}
	elseif ( strpos($url, 'youtu.be') ) {
		$matches = explode ('/' , $url );
		$youtu_code = $matches[3];
	}

	return $youtu_code;
}

$arResult['YOUTUBE'] = youtube_code($arResult['DISPLAY_PROPERTIES']['VIDEO']['~VALUE']);