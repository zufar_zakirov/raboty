<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

ob_start();

$disp_prop = $arResult["DISPLAY_PROPERTIES"];

$current_url = 'http://'.$_SERVER['HTTP_HOST'] . $APPLICATION->GetCurPage(true);
$summary = $disp_prop['TEXT_1']['~VALUE']['TEXT'];
$image = $disp_prop['IMAGE_ARTICLE']['FILE_VALUE']['SRC'];


if ($arResult["FIELDS"]["TAGS"] !=''){
	$tags_arr = explode(',', $arResult["FIELDS"]["TAGS"]);
}
?>

<?
// Создаем изображение для репостов соц сетей.  OpenGraf
$meta_og_img = CFile::ResizeImageGet( $arResult["DISPLAY_PROPERTIES"]["IMAGE_COVER"]["FILE_VALUE"]["ID"]
        , Array("width" => 600, "height" => 400), BX_RESIZE_IMAGE_EXACT, false);

$APPLICATION->SetPageProperty("meta_og_img",  $meta_og_img['src']);
?>


<div class="material__header jarallax" style="background-image: url(<?=$arResult["DISPLAY_PROPERTIES"]["IMAGE_COVER"]["FILE_VALUE"]["SRC"]?>)">

<div class="container">
                    <div class="material__header-content">
                        <div>
                            <time><?=$arResult["DISPLAY_ACTIVE_FROM"]?></time>
                        </div>
                        <h1><?=$arResult["NAME"]?></h1>
                        <div class="label">
                        <?php foreach ($tags_arr as $key => $value): ?>
                        	<a href="<?=SITE_DIR?>tags/?tag=<?=$value?>"><?=$value?></a>
                        <?php endforeach ?>
                        </div>
                    </div>
</div>
</div>

          <div class="material__wrap">
                <div class="container">
                    <div class="block-page__grid">
                        <div class="block-page__content">
                            <div class="panel-article">
                                <div class="panel-article__social">
			<a href="javascript: void(0)" data-sharer="facebook" data-url="<?=$current_url?>">
                                        <svg class="svg svg-facebook" width="50" height="50">
                                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/ico/sprite/sprite.svg#facebook"></use>
                                        </svg>
                                    </a>
                                    <a href="#in" data-sharer="linkedin" data-url="<?=$current_url?>">
                                        <svg class="svg svg-linkedin" width="50" height="50">
                                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/ico/sprite/sprite.svg#linkedin"></use>
                                        </svg>
                                    </a>
									<span><? echo GetMessage('SHARE'); ?></span>
                                </div>
                                <div class="panel-article__btn journal__footer">
                                    <a href="#envelope" class="journal__envelope js-share-email">
                                        <svg class="svg svg-envelope" width="50" height="50">
                                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/ico/sprite/sprite.svg#envelope"></use>
                                        </svg> <span><? echo GetMessage('SEND_EMAIL'); ?></span> 
                                    </a>
                                    <a href="#print" class="journal__printer js-print">
                                        <svg class="svg svg-printer" width="50" height="50">
                                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/ico/sprite/sprite.svg#printer"></use>
                                        </svg> <span><? echo GetMessage('PRINT'); ?></span> 
                                    </a>
                                    <a href="#pdf" class="journal__pdf">
                                        <svg class="svg svg-pdf" width="50" height="50">
                                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/ico/sprite/sprite.svg#pdf"></use>
                                        </svg> <span><? echo GetMessage('SAVE'); ?></span>
                                    </a>
                                </div>
                            </div>
                            <article>

<?php
    if ($arResult['YOUTUBE']):
?>
<div class="video">
<iframe width="640" height="360" src="https://www.youtube.com/embed/<?=$arResult['YOUTUBE']?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
<?php
    endif;
?>

<div class="cont_tei">
<?=$arResult['~TEXT_1'] ?>
</div>

<?php if ( 24 == $arResult['IBLOCK_SECTION_ID'] or 26 == $arResult['IBLOCK_SECTION_ID'] ): ?>

<div class="article-address">
            <div class="article-address__item"><b><? echo GetMessage('DATE_EVENT'); ?></b>  
                <span>
                    <?                                        
                    if ($TS_stamp = MakeTimeStamp($disp_prop['TIME_START']['VALUE'])) 
                    {                                         
                        echo date("d.m.Y (H:i - ", $TS_stamp); 
                    } 
                    if ($TS_stamp = MakeTimeStamp($disp_prop['TIME_END']['VALUE'])) 
                    {                                         
                        echo date(" H:i)", $TS_stamp); 
                    }

                    ?>

                </span>
                </div>
                <div class="article-address__item"><b><? echo GetMessage('LOCATION'); ?></b>  
                <span>
                   <?=$disp_prop['ADDRESS']['VALUE'];?>                                    
                </span>
             </div>
</div>
<?php endif; ?>                                
                                	
                                </p>
                            </article>
                        </div>
                        <div class="block-page__aside">
<? if( $disp_prop['AUTHORS'] !== NULL OR $disp_prop['AUTHORS_2'] !== NULL ) : ?>
    <h3><? echo GetMessage('AUTHORS'); ?></h3>                            
<? endif; ?>    
                            <div class="block-page__author">

<? if($disp_prop['AUTHORS'] !== NULL): ?>
                            

                            <? $k = 0;  ?>
                            <? foreach ($disp_prop['AUTHORS']['LINK_ELEMENT_VALUE'] as $key => $value):?>

                            <div class="block-page__author-item">

<?php
$id_author = $disp_prop['AUTHORS']['VALUE'][$k];
$id_iblock_author = $disp_prop['AUTHORS']["LINK_IBLOCK_ID"];

$arFilter = Array("IBLOCK_ID"=>$id_iblock_author, "ID"=>$id_author);
$res = CIBlockElement::GetList(Array(), $arFilter);
if ($ob = $res->GetNextElement()){;
    $arFields = $ob->GetFields(); // поля элемента    
    $arProps = $ob->GetProperties(); // свойства элемента    
}             
?>

                                <i><img src="<?=CFile::GetPath($value["PREVIEW_PICTURE"])?>" alt="IMG"></i>
                                    <div><b><?=$value['NAME']?></b>  
                                    <span>
                                    <?=$arProps['JOB_POSITION']['~VALUE']?>                                    
                                    </span>
                                    </div>
                                </div>
                            <? $k++; endforeach; ?>
                            
<? endif; ?>


<? if($disp_prop['AUTHORS_2'] !== NULL): ?>                                                        
                            <? $i = 0; ?>
                            <? foreach ($disp_prop['AUTHORS_2']['LINK_ELEMENT_VALUE'] as $key => $value):?>                            
<?php

$id_author = $disp_prop['AUTHORS_2']['VALUE'][$i];
$id_iblock_author = $disp_prop['AUTHORS_2']["LINK_IBLOCK_ID"];

$arFilter2 = Array("IBLOCK_ID"=>$id_iblock_author, "ID"=>$id_author);
$res = CIBlockElement::GetList(Array(), $arFilter2);
if ($ob = $res->GetNextElement() ){
    
    $arFields = $ob->GetFields(); // поля элемента    
    $arProps2 = $ob->GetProperties(); // свойства элемента    
}

$renderImage = CFile::ResizeImageGet($value["PREVIEW_PICTURE"], Array("width" => 101, "height" => 101), BX_RESIZE_IMAGE_EXACT, false); 

?>
                            <div class="block-page__author-item block">
                                <img src="<?=$renderImage['src']?>" align="left">
                                <p><b><?=$value['NAME']?></b><br />
                                    <?=$arProps2['JOB_POSITION']['~VALUE']?>                                    
                                    </p>                                    
                            </div>
                            <? $i++; endforeach; ?>                            
<? endif; ?>
</div>



                            <h3><? echo GetMessage('MOST_POPULAR'); ?></h3>

<div class="block-page__popular">
#INNER_BLOCK_MOST_POPULAR_1#

                            </div>
<?  $journal_id = $disp_prop['JOURNAL']['VALUE']; ?>
<?if($journal_id >0): ?>
                            <h3><? echo GetMessage('ISSUES'); ?></h3>
                            <div class="block-page__journal">
                                <div class="journal">
                                    <div class="journal__ico">
                                    <a href="<?=$disp_prop['JOURNAL']['LINK_ELEMENT_VALUE'][$journal_id]['DETAIL_PAGE_URL']?>"><i>
                                    <img src="<?=CFile::GetPath( $disp_prop['JOURNAL']['LINK_ELEMENT_VALUE'][$journal_id]['PREVIEW_PICTURE'] )?>" alt="IMG"></i>
                                    </a>
                                    </div>                                    
                                    <div class="journal__footer">
                                    <a href="<?=$disp_prop['JOURNAL']['LINK_ELEMENT_VALUE'][$journal_id]['DETAIL_PAGE_URL']?>" class="journal__download"><? echo GetMessage('READ_JOURNAL_ONLINE'); ?></a> 
                                        <a href="#pdf" class="journal__pdf">
                                            <svg class="svg svg-pdf" width="50" height="50">
                                                <use xlink:href="<?=SITE_TEMPLATE_PATH?>/ico/sprite/sprite.svg#pdf"></use>
                                            </svg>
                                        </a>
                                        <a href="#print" class="journal__printer">
                                            <svg class="svg svg-printer" width="50" height="50">
                                                <use xlink:href="<?=SITE_TEMPLATE_PATH?>/ico/sprite/sprite.svg#printer"></use>
                                            </svg>
                                        </a>
                                        <a href="#envelope" class="journal__envelope">
                                            <svg class="svg svg-envelope" width="50" height="50">
                                                <use xlink:href="<?=SITE_TEMPLATE_PATH?>/ico/sprite/sprite.svg#envelope"></use>
                                            </svg>
                                        </a>
                                    </div>
                                </div>
                            </div><a href="<?=SITE_DIR?>journal/ " class="btn"><span><? echo GetMessage('ALL_ISSUES'); ?></span></a>
<?endif;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>

<?
$this->__component->SetResultCacheKeys(array("CACHED_TPL"));
$this->__component->arResult["CACHED_TPL"] = @ob_get_contents();
ob_get_clean();
?>