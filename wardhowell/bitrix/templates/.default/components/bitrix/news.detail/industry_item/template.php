<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$disp_prop = $arResult["DISPLAY_PROPERTIES"];
?>
<article>
<h2><?=$arResult["NAME"]?></h2>
<p><?echo $arResult["DETAIL_TEXT"];?></p>
<!-- <h3>Сервисы</h3> -->
<dl>
<? foreach ($disp_prop['SERVICE_HEADER']['VALUE'] as $key => $value): ?>
            <dt><span><?=$value?></span></dt>
            <dd>
                    <p><?=$disp_prop['SERVICE_TEXT']['VALUE'][$key]['TEXT']?></p>
            </dd>
<? endforeach; ?>
</dl>
</article>