<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="school__box">
    <div class="school__title-box"><?=$arResult["NAME"]?></div>
    <?echo $arResult["DETAIL_TEXT"];?>
        <a class="school__link" href="#">Подать заявку<i> <svg class="svg svg-arrow" width="50" height="50">
        <use xlink:href="<?=SITE_TEMPLATE_PATH?>/ico/sprite/sprite.svg#arrow"></use></svg></i></a>
</div>