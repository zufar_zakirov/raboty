<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="school-our-work">
    <div class="container">
        <canvas id="school-our-work__canvas" width="184"></canvas>
        <div class="school__box">
            <div class="school__title-box"><?=$arResult["NAME"]?></div>

           <?echo $arResult["DETAIL_TEXT"];?>

        </div>
        <!-- /school__box -->
    </div>
    <!-- /container -->
</div>