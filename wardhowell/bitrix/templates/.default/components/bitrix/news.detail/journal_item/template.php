<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$disp_prop = $arResult["DISPLAY_PROPERTIES"];
?>
        <div class="release block-page">
            <div class="container">
                <div class="release__wrap">
                    <div class="release__desc">
                        <div class="release__desc-img">                            
<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["DETAIL_PICTURE"])):?>
		<i><img src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" alt="IMG"></i>
<?endif?>		
                            <div class="panel-btn">
                                <a href="#envelope" class="journal__envelope js-share-email">
                                    <svg class="svg svg-envelope" width="50" height="50">
                                        <use xlink:href="<?=SITE_TEMPLATE_PATH?>/ico/sprite/sprite.svg#envelope"></use>
                                    </svg> <span><? echo GetMessage('SEND_EMAIL'); ?></span> 
                                </a>
                                <a href="<?=$disp_prop["PDF_JOURNAL"]["FILE_VALUE"]["SRC"];?>" class="journal__printer js-print">
                                    <svg class="svg svg-printer" width="50" height="50">
                                        <use xlink:href="<?=SITE_TEMPLATE_PATH?>/ico/sprite/sprite.svg#printer"></use>
                                    </svg> <span><? echo GetMessage('PRINT'); ?></span> 
                                </a>
                                <a href="<?=$disp_prop["PDF_JOURNAL"]["FILE_VALUE"]["SRC"];?>" class="journal__pdf">
                                    <svg class="svg svg-pdf" width="50" height="50">
                                        <use xlink:href="<?=SITE_TEMPLATE_PATH?>/ico/sprite/sprite.svg#pdf"></use>
                                    </svg> <span><? echo GetMessage('SAVE'); ?></span>
                                </a>
                            </div>
                        </div>
                        <div class="release__desc-info">
                            <h2><?=$disp_prop['HEADER']['VALUE']?></h2>
                            <p><?=$disp_prop['TEXT']['~VALUE']['TEXT']?></p>




<? if( $disp_prop['AUTHORS'] !== NULL ): ?>
    <h3><? echo GetMessage('AUTHORS'); ?></h3>
<? endif; ?>
<div class="half author-block release__author">
<?
 if ( count( $disp_prop['AUTHORS']) >0 ): ?>
                            
    <? $i = 0;
         foreach ( $disp_prop['AUTHORS']['LINK_ELEMENT_VALUE'] as $key => $value ): ?>
                        <?
                            $id_author = $disp_prop['AUTHORS']['VALUE'][$i];
                            $id_iblock_author = $disp_prop['AUTHORS']["LINK_IBLOCK_ID"];

                            $arFilter2 = Array("IBLOCK_ID"=>$id_iblock_author, "ID"=>$id_author);
                            $res = CIBlockElement::GetList(Array(), $arFilter2);
                            if ($ob = $res->GetNextElement() ){
                                
                                $arFields = $ob->GetFields(); // поля элемента    
                                $arProps2 = $ob->GetProperties(); // свойства элемента    
                            }
                        ?>
                                <div class="author-block__item" href="<?=$value["DETAIL_PAGE_URL"]?>">
                                <i><img src="<?=CFile::GetPath($value["PREVIEW_PICTURE"])?>" alt="IMG"></i>
                                    <div><b><?=$value['NAME']?></b>
                                    <span><?=$arProps2['JOB_POSITION']['~VALUE']?></span>
                                    </div>
                                </div>
    <? $i++; endforeach; ?>

    <?  $i = 0; 
        foreach ( $disp_prop['OTHER_AUTHORS']['LINK_ELEMENT_VALUE'] as $key => $value ): ?>
                            <?
                            $id_author = $disp_prop['OTHER_AUTHORS']['VALUE'][$i];
                            $id_iblock_author = $disp_prop['OTHER_AUTHORS']["LINK_IBLOCK_ID"];

                            $arFilter2 = Array("IBLOCK_ID"=>$id_iblock_author, "ID"=>$id_author);
                            $res = CIBlockElement::GetList(Array(), $arFilter2);
                            if ($ob = $res->GetNextElement() ){
                                
                                $arFields = $ob->GetFields(); // поля элемента    
                                $arProps2 = $ob->GetProperties(); // свойства элемента    
                            }
                        ?>
                                <div class="author-block__item">
                                <i><img src="<?=CFile::GetPath($value["PREVIEW_PICTURE"])?>" alt="IMG"></i>
                                    <div><b><?=$value['NAME']?></b>
                                    <span><?=$arProps2['JOB_POSITION']['~VALUE']?></span>
                                    </div>
                                </div>
    <? $i++; endforeach; ?>
                            
<? endif; ?>
</div>





                        </div>
                    </div>
                </div>
            </div>
        </div>


