<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$disp_prop = $arResult["DISPLAY_PROPERTIES"];
$current_url = 'http://'.$_SERVER['HTTP_HOST'] . $APPLICATION->GetCurPage(true);

?>
<?php


if ($arResult["FIELDS"]["TAGS"] !=''){
	$tags_arr = explode(',', $arResult["FIELDS"]["TAGS"]);
}
?>
<div class="material__header event__header" style="background-image: url(<?=$arResult["DISPLAY_PROPERTIES"]["IMAGE_COVER"]["FILE_VALUE"]["SRC"]?>)">
            <div class="container">
                    <div class="material__header-content">
                        <div>
                            <time>Дата проведения: <?=$disp_prop['DATE']['VALUE']?></time>
                        </div>
                        <h1><?=$arResult["NAME"]?></h1>
                        <div class="label">
                        <?php foreach ($tags_arr as $key => $value): ?>
                        	<a href="#HR-Strategy"><?=$value?></a>
                        <?php endforeach ?>
                        </div>
                    </div>
            </div>
</div>

          <div class="material__wrap">
                <div class="container">
                    <div class="block-page__grid">
                        <div class="block-page__content">
                            <div class="panel-article">
                                <div class="panel-article__social">
                                    <a href="javascript: void(0)" data-sharer="facebook" data-url="<?=$current_url?>">
                                        <svg class="svg svg-facebook" width="50" height="50">
                                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/ico/sprite/sprite.svg#facebook"></use>
                                        </svg>
                                    </a>
                                    <!-- <a href="#tw">
                                        <svg class="svg svg-twitter" width="50" height="50">
                                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/ico/sprite/sprite.svg#twitter"></use>
                                        </svg>
                                    </a> -->
                                    <a href="#in" data-sharer="linkedin" data-url="<?=$current_url?>">
                                        <svg class="svg svg-linkedin" width="50" height="50">
                                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/ico/sprite/sprite.svg#linkedin"></use>
                                        </svg>
                                    </a>
                                    <span><? echo GetMessage('SHARE'); ?></span>
                                </div>
                                <div class="panel-article__btn journal__footer">
                                    <a href="#envelope" class="journal__envelope js-share-email">
                                        <svg class="svg svg-envelope" width="50" height="50">
                                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/ico/sprite/sprite.svg#envelope"></use>
                                        </svg> <span><? echo GetMessage('SEND_EMAIL'); ?></span> 
                                    </a>
                                    <a href="#print" class="journal__printer js-print">
                                        <svg class="svg svg-printer" width="50" height="50">
                                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/ico/sprite/sprite.svg#printer"></use>
                                        </svg> <span><? echo GetMessage('PRINT'); ?></span> 
                                    </a>
                                    <a href="#pdf" class="journal__pdf">
                                        <svg class="svg svg-pdf" width="50" height="50">
                                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/ico/sprite/sprite.svg#pdf"></use>
                                        </svg> <span><? echo GetMessage('SAVE'); ?></span>
                                    </a>
                                </div>
                            </div>
                            <article>        
								<?= $arResult['~DETAIL_TEXT'] ?>
                                


<? if (false == true): ?>
		<p><?=$disp_prop['TEXT_1']['~VALUE']['TEXT']?></p>
                            <? if ( !empty( $disp_prop['IMAGE_ARTICLE']['FILE_VALUE'] )): ?>
                                <div class="article-slider">
                                    <div class="article-slider__max js__slide-article-max">
                                    <? foreach ($disp_prop['IMAGE_ARTICLE']['FILE_VALUE'] as $key => $value): ?>
                                        <div>
                                        <img src="<?=$value['SRC']?>" alt="IMG">
                                        </div>
                                    <? endforeach; ?>
                                    </div>
                                    <div class="article-slider__min js__slide-article-min">
                                        <? foreach ($disp_prop['IMAGE_ARTICLE']['FILE_VALUE'] as $key => $value): ?>
                                            <div class="article-slider__min-item">
                                            <img src="<?=$value['SRC']?>" alt="IMG">
                                            </div>
                                        <? endforeach; ?>
                                    </div>
                                </div>
                            <? endif; ?>
<? endif; ?>                                



                                
	                  
                                

                                <div class="article-address">
                                    <div class="article-address__item"><b>Дата проведения</b>  
                                    <span>
                                        <?                                        
                                        if ($TS_stamp = MakeTimeStamp($disp_prop['TIME_START']['VALUE'])) 
                                        {                                         
                                            echo date("d.m.Y (H:i - ", $TS_stamp); 
                                        } 
                                        if ($TS_stamp = MakeTimeStamp($disp_prop['TIME_END']['VALUE'])) 
                                        {                                         
                                            echo date(" H:i)", $TS_stamp); 
                                        }

                                        ?>

                                    </span>
                                    </div>
                                    <div class="article-address__item"><b>Место проведения</b>  
                                    <span>
                                    <?=$disp_prop['ADDRESS']['VALUE'];?>                                    
                                    </span>
                                    </div>
                                </div>
                            </article>
                        </div>
<? if ($disp_prop['AUTHORS']['LINK_ELEMENT_VALUE']): ?>
                        <div class="block-page__aside">
                            <h3>Авторы</h3>                            
                            <div class="block-page__author">

                            <? //var_dump( $disp_prop['AUTHORS']['LINK_ELEMENT_VALUE'] ) ?>
                            <? foreach ($disp_prop['AUTHORS']['LINK_ELEMENT_VALUE'] as $key => $value):?>

                            <div class="block-page__author-item">

                            <?
                            //echo CFile::ShowImage($value['PREVIEW_PICTURE'], 200, 200, "border=0", "", true);
                            ?>

                                <i><img src="<?=CFile::GetPath($value["PREVIEW_PICTURE"])?>" alt="IMG"></i>
                                    <div><b><?=$value['NAME']?></b>  
                                    <span>Старший партнер. Председатель совета...</span>
                                    </div>
                                </div>
                            <? endforeach; ?>

                            </div>
                    </div>
<? endif; ?>

                </div>
            </div>
        </div>

</div>