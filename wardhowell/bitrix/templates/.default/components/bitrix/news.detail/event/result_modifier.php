<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$galleryHtml = '';
$imageFieldName = 'IMAGE_ARTICLE';
$text_1 = $arResult["DISPLAY_PROPERTIES"]['TEXT_1']['~VALUE']['TEXT'];

if (! empty($arResult["DISPLAY_PROPERTIES"][$imageFieldName]['FILE_VALUE'])) {
	$galleryImageField = &$arResult["DISPLAY_PROPERTIES"][$imageFieldName];
	
	$galleryHtml .= '<div class="article-slider"><div class="article-slider__max js__slide-article-max">';
	
	foreach ($galleryImageField['FILE_VALUE'] as $key => $value) {
		$galleryHtml .= '<div><img src="'.$value['SRC'].'" alt="" /></div>';
	}
	$galleryHtml .= '</div><div class="article-slider__min js__slide-article-min">';
	foreach ($galleryImageField['FILE_VALUE'] as $key => $value) {
		$galleryHtml .= '<div class="article-slider__min-item"><img src="'.$value['SRC'].'" alt="" /></div>';
	}
	
	$galleryHtml .= '</div></div>';
}

$arResult['~DETAIL_TEXT'] = str_replace('#GALLERY#', $galleryHtml, $text_1);