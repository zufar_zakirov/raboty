<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$disp_prop = $arResult["DISPLAY_PROPERTIES"];
?>
<?
// Создаем изображение для репостов соц сетей.  OpenGraf
$meta_og_img = CFile::ResizeImageGet( $arResult['DETAIL_PICTURE']["ID"]
        , Array("width" => 600, "height" => 400), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, false);

$APPLICATION->SetPageProperty("meta_og_img",  $meta_og_img['src']);
?>
<div class="consultant-p block-page">
            <div class="container">               

                <div class="consultant-p__wrap">
                    <div class="consultdesc">
                        <div class="consultdesc__img">
                            <h2><?=$arResult["NAME"]?></h2>
                            <img src="<?=$arResult['DETAIL_PICTURE']['SRC']?>" alt="consultdesc">
                        </div>
                        <? //var_dump( $arResult['DETAIL_PICTURE']['SRC'] ) ?>
                        <div class="consultdesc__info">
                            <h2><?=$arResult["NAME"]?></h2>
                            <span><?=$disp_prop['JOB_POSITION']['VALUE']?></span>
                            <a href="mailto:<?=$disp_prop['EMAIL']['VALUE']?>"><?=$disp_prop['EMAIL']['VALUE']?></a>

<? if ($disp_prop['INDUSTRY'] !== NULL): ?>
                                <h3><?=$disp_prop['INDUSTRY']['NAME']?>:</h3>
                                <ul>                            
    		      <?foreach ($disp_prop['INDUSTRY']['LINK_ELEMENT_VALUE'] as $key => $value):?>
    			<li><a href="<?=$value['DETAIL_PAGE_URL']?>">
    			<?=$value['NAME']?>
    			</a></li>	
    		      <? endforeach; ?>
                                </ul>
<? endif; ?>

<? if ($disp_prop['OFFICE'] !== NULL): ?>
                            <h3><?=$disp_prop['OFFICE']['NAME']?>:</h3>                            
                            <ul>                            
                                <li><span><?=$disp_prop["OFFICE"]['VALUE']?></span>
                                </li>
                            </ul>
<? endif; ?>


<? if ($disp_prop['EDUCATION'] !== NULL): ?>
                            <h3><?=$disp_prop['EDUCATION']['NAME']?>:</h3>
                            <ul>
                                <?foreach ($disp_prop['EDUCATION']['VALUE'] as $key => $value):?>
			<li><span>
			<?=$value?>
			</span></li>	
        		      <? endforeach; ?>
                            </ul>
<? endif; ?>


<? if ($disp_prop['LANGUAGES'] !== NULL): ?>
                            <h3><?=$disp_prop['LANGUAGES']['NAME']?>:</h3>
                            <ul>
                            <?foreach ($disp_prop['LANGUAGES']['VALUE'] as $key => $value):?>
			<li><span>
			<?=$value?>
			</span></li>	
		<? endforeach; ?>                                
                            </ul>
<? endif; ?>
		
                            <?=$arResult['DETAIL_TEXT']?>

                        </div>
                        </div>
                        </div>
                        </div>

</div>







