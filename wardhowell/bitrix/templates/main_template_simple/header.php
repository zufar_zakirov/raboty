<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!DOCTYPE html>

<html lang="ru" class="page-index page-school">
<head>
    <meta charset="utf-8">
    <title><?$APPLICATION->ShowTitle()?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1">
    <?
        $APPLICATION->ShowHead();

        CJSCore::Init(['ajax']);

        \Bitrix\Main\Page\Asset::getInstance()->addCss('/bitrix/templates/main_template_simple/css/main.css');
        \Bitrix\Main\Page\Asset::getInstance()->addCss('/bitrix/templates/main_template_simple/css/style.css');
		\Bitrix\Main\Page\Asset::getInstance()->addCss('/bitrix/templates/.default/vendor/jarallax/jarallax.css');
		
		\Bitrix\Main\Page\Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/main.js?v=2');
		\Bitrix\Main\Page\Asset::getInstance()->addJs('/bitrix/templates/.default/vendor/jarallax/jarallax.min.js');
		\Bitrix\Main\Page\Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/include/js.cookie.js');
		\Bitrix\Main\Page\Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/include/js.js');
	?>
    <link rel="shortcut icon" href="/favicon.ico">
    <link rel="stylesheet" href="/bitrix/templates/main_template/css/print.css?v=2" media="print">
    <!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '341693143225982');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=341693143225982&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->




<meta property = "og:title" content = "<?$APPLICATION->ShowTitle()?>" />
<meta property = "og:type" content = "website" />

<? if ($APPLICATION->GetCurPage(false) === '/'): ?>
<meta property = "og:image" content = "http://<?echo $_SERVER['SERVER_NAME'] ?>/img/main_og.jpg" />
<? else: ?>
<meta property = "og:image" content = "http://<?echo $_SERVER['SERVER_NAME'] ?><?php $APPLICATION->ShowProperty("meta_og_img"); ?>" />

<? endif; ?>

<?
global $USER;
if ($USER->IsAdmin()):
?>
<style type="text/css">
  .page-school .menu{
    z-index: 1;
  }
</style>
<? endif; ?>

</head>

<body>
<div id="panel"><?$APPLICATION->ShowPanel();?></div>
    <div id="wrapper">
<?
if(
                       $APPLICATION->GetCurPage() != "/alumni.php"
              AND  $APPLICATION->GetCurPage() != "/philosophy.php"
              AND  $APPLICATION->GetCurPage() != "/about/philosophy.php"
              AND  $APPLICATION->GetCurPage() != "/about/alumni.php"
              AND  $APPLICATION->GetCurPage() != "/about/cultura.php"
              AND  $APPLICATION->GetCurPage() != "/about/prof.php"
              AND  $APPLICATION->GetCurPage() != "/about/career.php"

                            
              AND strpos( $APPLICATION->GetCurPage(), "about/tes") === false
              
              AND strpos( $APPLICATION->GetCurPage(), "philosophy.php") === false
              AND strpos( $APPLICATION->GetCurPage(), "/event/") === false

              AND strpos( $APPLICATION->GetCurPage(), "/tags/") === false
              //AND  $APPLICATION->GetCurPage() != "" 


              AND  $APPLICATION->GetCurPage() != "/en/"
              AND  $APPLICATION->GetCurPage() != "/"
              
              AND strpos( $APPLICATION->GetCurPage(), "/teinstitute/") === false
              //AND strpos( $APPLICATION->GetCurPage(), "/en/about/") === false
              AND strpos( $APPLICATION->GetCurPage(), "/media/") === false
              
):?>

        <div class="container">        
                <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "breadcrupmb", Array(
	"PATH" => "",	// Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
		"SITE_ID" => "s1",	// Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
		"START_FROM" => 0, // Номер пункта, начиная с которого будет построена навигационная цепочка
	),
	false );?>
        </div>
<?endif;?>

        <nav class="nav">

            <div class="nav__close js__nav-close"><i><svg class="svg svg-close" width="50" height="50">
            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/ico/sprite/sprite.svg#close"></use></svg></i>
            </div>
            <div class="nav__wrap js__scroll-nav">

<?$APPLICATION->IncludeComponent(
    "bitrix:menu", 
    "menu_left", 
    array(
        "ROOT_MENU_TYPE" => "leftfirst",
        "MENU_CACHE_TYPE" => "Y",
        "MENU_CACHE_TIME" => "36000000",
        "MENU_CACHE_USE_GROUPS" => "Y",
        "MENU_CACHE_GET_VARS" => array(
        ),
        "MAX_LEVEL" => "3",
        "CHILD_MENU_TYPE" => "left",
        "USE_EXT" => "Y",
        "ALLOW_MULTI_SELECT" => "N",
        "COMPONENT_TEMPLATE" => "menu_left",
        "DELAY" => "N"
    ),
    false
);?>

<?php 
$title=urlencode('WardHowell');
$url=urlencode('http://wardhowell.com');
$summary=urlencode('Ward Howell – консалтинговая компания по поиску руководителей и развитию лидерского капитала. Подробнее - на www.wardhowell.com');
$image=urlencode('http://ward.riott.ru/img/wh_n.png');
$parser = file_get_contents('http://graph.facebook.com/?id='. $url .'');
$arr_fb = json_decode($parser, true);
$fb_count = $arr_fb['share']['share_count'];
?>
<?
$css_eng = ''; $css_rus = '';
if (SITE_ID == 's2'){
    $css_eng = 'active';
}else{
    $css_rus = 'active';
}  ?>

<div class="nav__footer">
        <div class="nav__lang"><a href="/" class="<?=$css_rus?>">RUS</a>  <a class="<?=$css_eng?>" href="/en/">ENG</a>
                    </div>
<?$APPLICATION->IncludeComponent(
	"ward:subscribe",
	"menu",
	array(),
	false
);?>

                    <div class="nav__footer-line">                    
                    <iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FWardHowellTalentEquity&width=129&layout=button_count&action=like&size=small&show_faces=false&share=false&height=21&appId" width="129" height="21" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>

                        <a href="/">
                            <svg class="svg svg-logo" width="50" height="50">
                                <use xlink:href="<?=SITE_TEMPLATE_PATH?>/ico/sprite/sprite.svg#logo"></use>
                            </svg>
                        </a>
      </div>
</div>

</div>
</nav>



<?if($APPLICATION->GetCurPage() == "/"){
    $class_page = 'menu--transparent';
}else{
    $class_page = 'menu--blue';
}
?>


        <div class="menu <?=$class_page?>">
            <div class="container">
                <div class="menu__wrap">
                    <div class="menu__group menu__group--mobile">
                        <div class="menu__btn js__open-nav"><span><i></i> <i></i> <i></i></span>
                        </div>
                        <div class="menu__logo"><a href="<?=SITE_DIR?>"><i><svg class="svg svg-logo" width="50" height="50">
                        <use xlink:href="<?=SITE_TEMPLATE_PATH?>/ico/sprite/sprite.svg#logo"></use></svg> </i><span><?= $APPLICATION->GetProperty("header_head"); ?></span></a>
                        </div>
                        <div class="menu__mobile-search">
                            <svg class="svg svg-search search-open" width="50" height="50">
                                <use xlink:href="/bitrix/templates/main_template/ico/sprite/sprite.svg#search"></use>
                            </svg>
                            <svg class="svg svg-close search-close" width="50" height="50">
                                <use xlink:href="/bitrix/templates/main_template/ico/sprite/sprite.svg#close"></use>
                            </svg>
                        </div>
                    </div>
                    <div class="menu__group menu__group--desktop">
                        <div class="menu__text"><a href="<?=SITE_DIR?>teinstitute/"><span>Talent Equity Institute</span></a>
                        </div>

<?$APPLICATION->IncludeComponent(
    "bitrix:search.form",
    "search_header",
    Array(
        "PAGE" => "#SITE_DIR#search/index.php",
        "USE_SUGGEST" => "N"
    )
);?>
                        <div class="menu__lang"><a href="/" class="<?=$css_rus?>">RUS</a>  <a href="/en/" class="<?=$css_eng?>">ENG</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>