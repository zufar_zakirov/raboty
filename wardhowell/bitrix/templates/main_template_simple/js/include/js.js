jQuery(document).ready(function(){

	$('.nav__item-submenu a').click( function(event){
		event.defaultPrevented();
	} );

	$('.ico_search').click( function(e){		
		e.defaultPrevented();
	} );


	$('.tab-sp').click( function (e){
		$('.city-ms').hide();
		$('.city-sp').show();
		
		$('.map-ms').hide();
		$('.map-sp').show();
		$('.office__team').hide();
	});

	$('.tab-ms').click( function (e){
		$('.city-sp').hide();
		$('.city-ms').show();

		$('.map-ms').show();
		$('.map-sp').hide();
		$('.office__team').show();
	});
	

	$(document).ready(function() {
	
		if($(".slick-track").is(":empty")) {	
			$(".news").hide();		
		}		
	});

	$('.noUi-base').click( function(){
		$radio  = $('.noUi-handle').attr('aria-valuenow');

		switch ($radio) {
			case '3.0':
				$('#id_69').prop("checked", true);  
				break;
			case '25.0':
			    	$('#id_70').prop("checked", true); 					
			    	break;
			case '50.0':
				$('#id_71').prop("checked", true); 
			    	break;
			 case '75.0':
				$('#id_72').prop("checked", true); 
			    	break;
			case '95.0':
				$('#id_73').prop("checked", true); 
			    	break;
		}

	} );


	$('.field-checkbox').click(function(){
		
		if ( $('#id_76').prop("checked") ){
			$('#id_76').prop("checked", false); 
		}else{
			$('#id_76').prop("checked", true); 
		}

		console.log( $('#id_76').prop("checked") );

	});



	// $.post('/include/ajax/intership.php', function(data) {
	// 	$('.school-order__add').html(data);		  	
	// });


	
	



});

(function($) {
	$(function() {
		if (! Cookies.get('wh-cookie-accept') && $('.js-cookie-accept').length) {
            $('.js-cookie-accept').show();
            $('.js-cookie-accept .btn').on('click', function () {
                Cookies.set('wh-cookie-accept', 'Y', { expires: 365 });
                $(this).closest('.js-cookie-accept').fadeOut('fast');
            });
        }
		
		$(".js-request-form").fancybox({
			baseClass: 'fancybox-request-form',
			type: 'ajax',
			modal: true,
			src: '/include/ajax/request.php',
			afterShow: function( instance, current ) {
				initModalFileInput();
			},
			afterClose: function( instance, current ) {
				$('body').removeClass('fancybox-active compensate-for-scrollbar');
			}
		});
		
		$("body").on('click', '.js-policy-form', function(e) {
			e.preventDefault();
			
			$.fancybox.open({
				baseClass: 'fancybox-request-form',
				type: 'ajax',
				modal: true,
				src: '/include/ajax/policy.php',
				afterClose: function( instance, current ) {
					$('body').removeClass('fancybox-active compensate-for-scrollbar');
				}
			});
		});
		$(".js-webform[data-form]").on('click', function(e) {
			e.preventDefault();
			
			var formId = parseInt($(this).attr('data-form'));
			
			$.fancybox.open({
				baseClass: 'fancybox-request-form',
				type: 'ajax',
				modal: true,
				src: '/include/ajax/webform.php',
				ajax: {
					settings: {
						type: 'POST',
						data: {
							WEB_FORM_ID: formId,
							fancybox: true
						}
					}
				},
				afterShow: function( instance, current ) {
					initModalFileInput();
				},
				afterClose: function( instance, current ) {
					$('body').removeClass('fancybox-active compensate-for-scrollbar');
				}
			});
		});
		
		$(".js-share-email").on('click', function(e) {
			e.preventDefault();
			
			var title = $(this).attr('data-title') || $('h1').text();
			var url = window.location.href;
			if ($(this).attr('data-url')) {
				url = location.origin+$(this).attr('data-url');
			}
			
			$.fancybox.open({
				baseClass: 'fancybox-request-form',
				type: 'ajax',
				modal: true,
				src: '/include/ajax/share_email.php',
				ajax: {
					settings: {
						type: 'POST',
						data: {
							title: title,
							url: url,
							fancybox: true
						}
					}
				},
				afterShow: function( instance, current ) {
					current.$content.on('submit', 'form', function(e) {
						e.preventDefault();
						$.ajax({
							type: "POST",
							url: $(this).attr('action'),
							data: $(this).serialize(),
							dataType: 'html',
							success: function(data) {
								$(current.$content).html( data );
							}
						});
					});
				},
				afterClose: function( instance, current ) {
					$('body').removeClass('fancybox-active compensate-for-scrollbar');
				}
			});
		});
		
		$('.js-print').on('click', function(e) {
			e.preventDefault();
			window.print();
		});
		if ('#print' == window.location.hash) {
			window.print();
		}
		
		$('.journal__pdf[href="#pdf"]').on('click', function(e) {
			e.preventDefault();
			window.location.href = window.location.href + '?pdf=1';
		});
		
		$('[data-goto]').on('click', function(e) {
			e.preventDefault();
			
			$('html, body').animate({
				scrollTop: $($(this).attr('data-goto')).offset().top
            }, 750);
		});
	});

	function initModalFileInput()
	{
		$(".js-text-policy").html('Согласен с <a href="/privacy/">политикой конфиденциальности<a/> сайта');
		$("[data-input-file] input").off("change").on("change", function () {
			if ($(this).val()) {
				$(this).parent().find("[data-input-file-selected]").text($(this).val());
			} else {
				$(this).parent().find("[data-input-file-selected]").text("Файл не выбран");
			}
		});
	}
	
	initModalFileInput();
	BX.addCustomEvent('onAjaxSuccess', initModalFileInput);
})(jQuery);


