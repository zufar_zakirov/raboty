        <footer class="footer">
            <div class="container">
                <div class="footer__top">
                    <div class="footer__logo">
                        <a href="/">
                            <img src="<?=SITE_TEMPLATE_PATH?>/ico/logo.png" alt="IMG">
                        </a>
                    </div>
                    <div class="footer__nav">
<?$APPLICATION->IncludeComponent(
    "bitrix:menu", 
    "bottom", 
    array(
        "ROOT_MENU_TYPE" => "bottom",
        "MENU_CACHE_TYPE" => "Y",
        "MENU_CACHE_TIME" => "36000000",
        "MENU_CACHE_USE_GROUPS" => "Y",
        "MENU_CACHE_GET_VARS" => array(
        ),
        "MAX_LEVEL" => "1",
        "CHILD_MENU_TYPE" => "left",
        "USE_EXT" => "Y",
        "ALLOW_MULTI_SELECT" => "N",
        "COMPONENT_TEMPLATE" => "bottom",
        "DELAY" => "N"
    ),
    false
);?>
                    </div>
                    <div class="footer__social">
                        <iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FWardHowellTalentEquity&width=129&layout=button_count&action=like&size=small&show_faces=false&share=false&height=21&appId" width="129" height="21" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                        
                    </div>
                </div>

                <div class="footer__copyright">

                    <div class="footer__copyright-text"><span>© Ward Howell. <?=date( 'Y', time() );?>
                    <br><? echo GetMessage('RIGHTS'); ?></span>
                    </div>
                    <div class="footer__copyright-link"><a href="tel:+7495921-29-01">+ 7 495 921-29-01</a>
                        <br><a href="mailto:info@wardhowell.com">info@wardhowell.com</a>
                    </div>

                    <div class="footer__copyright-img__links footer__copyright-link">
                        <a href="<?=SITE_DIR?>privacy/"><? echo GetMessage('PRIVACY'); ?></a>
                        <br />
                        <a href="#" class="js-policy-form" rel="nofollow"><? echo GetMessage('CONCENT_FORM'); ?></a>                        
                    </div>

                    <div class="footer__copyright-img">
                        <img src="<?=SITE_TEMPLATE_PATH?>/img/aesc.png" alt="">                    
                        <img src="<?=SITE_TEMPLATE_PATH?>/img/bluesteps.png" class="img_bluesteps">
                    </div>

                    <div class="footer__copyright-dev">
			<a href="https://itargency.ru/"><em><? echo GetMessage('SITE_DEVELOP'); ?> </em><img src="<?=SITE_TEMPLATE_PATH?>/ico/iTargency.png" alt=""></a>
                    </div>

<?$APPLICATION->IncludeFile(
            $APPLICATION->GetTemplatePath("/include/html/count.php"),
            Array(),
            Array("MODE"=>"html")
            );
?>
              

                </div> 

            </div>
        </footer>
    </div>
	
    <script>

    // $('.scroll-y').perfectScrollbar();    // Initialize
    // $('.scroll-y').perfectScrollbar({ ... });   // with options
    // $('.scroll-y').perfectScrollbar('update');  // Update
    // $('.scroll-y').perfectScrollbar('destroy'); // Destroy




		jarallax(document.querySelectorAll('.jarallax'), {
			speed: 0.2
		});
        svg4everybody();
    </script>
	<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/cookie-accept.php"), false);?>



</body>

</html>