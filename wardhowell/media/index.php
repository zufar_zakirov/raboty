<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Медиацентр ");
?>


        <div class="media block-page pt-5">
            <div class="container">
                <div class="media__wrap">
                    <h2>Медиацентр</h2>

                    <div class="media__grid">

<?
//$GLOBALS['arrFilter'] = array("PROPERTY_SHOW_MASS_MEDIA" => 'YES');
$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"mass_media_list2", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "mass_media_list",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "CODE",
			1 => "XML_ID",
			2 => "NAME",
			3 => "TAGS",
			4 => "SORT",
			5 => "PREVIEW_TEXT",
			6 => "PREVIEW_PICTURE",
			7 => "DETAIL_TEXT",
			8 => "DETAIL_PICTURE",
			9 => "DATE_ACTIVE_FROM",
			10 => "ACTIVE_FROM",
			11 => "DATE_ACTIVE_TO",
			12 => "ACTIVE_TO",
			13 => "SHOW_COUNTER",
			14 => "SHOW_COUNTER_START",
			15 => "IBLOCK_TYPE_ID",
			16 => "IBLOCK_ID",
			17 => "IBLOCK_CODE",
			18 => "IBLOCK_NAME",
			19 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "36",
		"IBLOCK_TYPE" => "Catalog",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "4",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "more",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "110",
		"PROPERTY_CODE" => array(
			0 => "TEXT_1",
			1 => "CONCLUSION_TEXT",
			2 => "VIDEO",
			3 => "SHOW_MASS_MEDIA",
			4 => "IMAGE_COVER",
			5 => "IMAGE_COVER",
			6 => "NAME_MEDIA",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N"
	),
	false
);?>
                        
                    </div>                    
                </div>
            </div>
        </div>
        <div class="subscribe">
            <div class="container">
                <div class="subscribe__wrap">
                           <?$APPLICATION->IncludeFile(
		            $APPLICATION->GetTemplatePath("/include/html/contact_us.php"),
		            Array(),
		            Array("MODE"=>"html")
		            );
		?>         
		<?$APPLICATION->IncludeComponent(
			"ward:subscribe",
			"form",
			array(),
			false
		);?>
                </div>
            </div>
        </div>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>