<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Title");
?>
<div class="material block-page">
<?
$news_id = $APPLICATION->IncludeComponent(
	"bitrix:news.detail", 
	"tei_item", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_ELEMENT_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"BROWSER_TITLE" => "-",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "tei_item",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_CODE" => $_REQUEST["CODE"],
		"ELEMENT_ID" => "",
		"FIELD_CODE" => array(
			0 => "NAME",
			1 => "TAGS",
			2 => "DETAIL_TEXT",
			3 => "DETAIL_PICTURE",
			4 => "DATE_CREATE",
			5 => "",
		),
		"IBLOCK_ID" => "1",
		"IBLOCK_TYPE" => "Catalog",
		"IBLOCK_URL" => "",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"MESSAGE_404" => "404 Not ",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Страница",
		"PROPERTY_CODE" => array(
			0 => "TEXT_1",
			1 => "CONCLUSION_TEXT",
			2 => "VIDEO",
			3 => "TYPE",
			4 => "TEXT_HEAD_1",
			5 => "QUOTE",
			6 => "AUTHOR_QUOTE",
			7 => "AUTHOR_POSITION",
			8 => "LISTS",
			9 => "TEXT_2",
			10 => "QUOTE_2_HEAD",
			11 => "QUOTE_2",
			12 => "TEXT_HEAD_3",
			13 => "TEXT_3",
			14 => "TASKS",
			15 => "RESULTS_TASK",
			16 => "PERIOD",
			17 => "PRICE",
			18 => "CONCLUSION_NUMBER",
			19 => "CONCLUSION_HEAD",
			20 => "TEXT_4",
			21 => "Text_4",
			22 => "IMAGE_ARTICLE",
			23 => "IMAGE_COVER",
			24 => "AUTHORS",
			25 => "JOURNAL",
			26 => "CONCLUSION_TEXT",
			27 => "AUTHORS_2",
		),
		"SET_BROWSER_TITLE" => "Y",
		"SET_CANONICAL_URL" => "N",
		"SET_LAST_MODIFIED" => "Y",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "Y",
		"SET_TITLE" => "Y",
		"SHOW_404" => "Y",
		"STRICT_SECTION_CHECK" => "N",
		"USE_PERMISSIONS" => "N",
		"USE_SHARE" => "N"
	),
	false
);?><br>
 <br>
 <?
 // Исключаем этот же элемент из списка.
 $filt_id = array("!=ID" => $news_id);

// Фильтруем по тегам.
$res = CIBlockElement::GetByID( $news_id );
$ar_res = $res->GetNext();
if( !empty($ar_res["TAGS"]) ){
	$tags = explode( ',' , $ar_res["TAGS"] );	

	foreach ($tags as $tag) {
		if( strlen($tag)>1 ){
			$tag = trim($tag);
			$filt[] = array("TAGS" => "%" . $tag . "%");
		}
	}
	
	$tmpArray=array("LOGIC" => "OR");
	$tmpArray=array_merge( $tmpArray, $filt );
	$addFArray=array(
     		array($tmpArray),
    	);
	$GLOBALS['arrFilter2'] = array_merge( $filt_id, $addFArray ); 
}else{
	$GLOBALS['arrFilter2'] =  $filt_id;
}
?>

<?
$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"news_main", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "CODE",
			1 => "XML_ID",
			2 => "NAME",
			3 => "TAGS",
			4 => "SORT",
			5 => "PREVIEW_TEXT",
			6 => "PREVIEW_PICTURE",
			7 => "DETAIL_TEXT",
			8 => "DETAIL_PICTURE",
			9 => "DATE_ACTIVE_FROM",
			10 => "ACTIVE_FROM",
			11 => "DATE_ACTIVE_TO",
			12 => "ACTIVE_TO",
			13 => "SHOW_COUNTER",
			14 => "SHOW_COUNTER_START",
			15 => "IBLOCK_TYPE_ID",
			16 => "IBLOCK_ID",
			17 => "IBLOCK_CODE",
			18 => "IBLOCK_NAME",
			19 => "IBLOCK_EXTERNAL_ID",
			20 => "DATE_CREATE",
			21 => "CREATED_BY",
			22 => "CREATED_USER_NAME",
			23 => "TIMESTAMP_X",
			24 => "MODIFIED_BY",
			25 => "USER_NAME",
			26 => "",
		),
		"FILTER_NAME" => "arrFilter2",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "1",
		"IBLOCK_TYPE" => "Catalog",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "10",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Материалы по теме",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "95",
		"PROPERTY_CODE" => array(
			0 => "TEXT_1",
			1 => "CONCLUSION_TEXT",
			2 => "VIDEO",
			3 => "TYPE",
			4 => "TEXT_HEAD_1",
			5 => "QUOTE",
			6 => "AUTHOR_QUOTE",
			7 => "AUTHOR_POSITION",
			8 => "LISTS",
			9 => "TEXT_2",
			10 => "QUOTE_2_HEAD",
			11 => "QUOTE_2",
			12 => "TEXT_HEAD_3",
			13 => "TEXT_3",
			14 => "TASKS",
			15 => "RESULTS_TASK",
			16 => "PERIOD",
			17 => "PRICE",
			18 => "CONCLUSION_NUMBER",
			19 => "CONCLUSION_HEAD",
			20 => "TEXT_4",
			21 => "IMAGE_COVER",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"COMPONENT_TEMPLATE" => "tei_related"
	),
	false
);?>



       <div class="subscribe">
            <div class="container">
                <div class="subscribe__wrap">

<?$APPLICATION->IncludeFile(
            $APPLICATION->GetTemplatePath("/include/html/contact_us.php"),
            Array(),
            Array("MODE"=>"html")
            );
?>           
<?$APPLICATION->IncludeComponent(
	"ward:subscribe",
	"form",
	array(),
	false
);?>
                </div>
            </div>
        </div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>