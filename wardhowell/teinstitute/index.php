<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Talent Equity Institute - Ward Howell");
?>


<?
 $APPLICATION->IncludeComponent("bitrix:news.list", "banner_teinstitute2", 
 Array(
    "ACTIVE_DATE_FORMAT" => "d.m.Y",    // Формат показа даты
        "ADD_SECTIONS_CHAIN" => "N",    // Включать раздел в цепочку навигации
        "AJAX_MODE" => "N", // Включить режим AJAX
        "AJAX_OPTION_ADDITIONAL" => "", // Дополнительный идентификатор
        "AJAX_OPTION_HISTORY" => "N",   // Включить эмуляцию навигации браузера
        "AJAX_OPTION_JUMP" => "N",  // Включить прокрутку к началу компонента
        "AJAX_OPTION_SHADOW" => "Y",
        "AJAX_OPTION_STYLE" => "N", // Включить подгрузку стилей
        "CACHE_FILTER" => "N",  // Кешировать при установленном фильтре
        "CACHE_GROUPS" => "Y",  // Учитывать права доступа
        "CACHE_TIME" => "36000000", // Время кеширования (сек.)
        "CACHE_TYPE" => "A",    // Тип кеширования
        "CHECK_DATES" => "Y",   // Показывать только активные на данный момент элементы
        "DETAIL_URL" => "", // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
        "DISPLAY_BOTTOM_PAGER" => "N",  // Выводить под списком
        "DISPLAY_DATE" => "Y",  // Выводить дату элемента
        "DISPLAY_NAME" => "Y",  // Выводить название элемента
        "DISPLAY_PANEL" => "N",
        "DISPLAY_PICTURE" => "Y",   // Выводить изображение для анонса
        "DISPLAY_PREVIEW_TEXT" => "Y",  // Выводить текст анонса
        "DISPLAY_TOP_PAGER" => "N", // Выводить над списком
        "FIELD_CODE" => array(  // Поля
            0 => "NAME",
            1 => "DETAIL_PICTURE",
            2 => "",
        ),
        "FILTER_NAME" => "",    // Фильтр
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",  // Скрывать ссылку, если нет детального описания
        "IBLOCK_ID" => "5", // Код информационного блока
        "IBLOCK_TYPE" => "Catalog", // Тип информационного блока (используется только для проверки)
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N", // Включать инфоблок в цепочку навигации
        "INCLUDE_SUBSECTIONS" => "Y",   // Показывать элементы подразделов раздела
        "MESSAGE_404" => "",    // Сообщение для показа (по умолчанию из компонента)
        "NEWS_COUNT" => "1",   // Количество новостей на странице
        "PAGER_BASE_LINK_ENABLE" => "N",    // Включить обработку ссылок
        "PAGER_DESC_NUMBERING" => "N",  // Использовать обратную навигацию
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000000",    // Время кеширования страниц для обратной навигации
        "PAGER_SHOW_ALL" => "N",    // Показывать ссылку "Все"
        "PAGER_SHOW_ALWAYS" => "N", // Выводить всегда
        "PAGER_TEMPLATE" => "", // Шаблон постраничной навигации
        "PAGER_TITLE" => "Новости", // Название категорий
        "PARENT_SECTION" => "21", // ID раздела
        "PARENT_SECTION_CODE" => "",    // Код раздела
        "PREVIEW_TRUNCATE_LEN" => "",   // Максимальная длина анонса для вывода (только для типа текст)
        "PROPERTY_CODE" => array(   // Свойства
            0 => "TEXT_HREF",
            1 => "URL",
            2 => "BANNER",
            3 => "",
        ),
        "SET_BROWSER_TITLE" => "N", // Устанавливать заголовок окна браузера
        "SET_LAST_MODIFIED" => "N", // Устанавливать в заголовках ответа время модификации страницы
        "SET_META_DESCRIPTION" => "N",  // Устанавливать описание страницы
        "SET_META_KEYWORDS" => "N", // Устанавливать ключевые слова страницы
        "SET_STATUS_404" => "N",    // Устанавливать статус 404
        "SET_TITLE" => "N", // Устанавливать заголовок страницы
        "SHOW_404" => "N",  // Показ специальной страницы
        "SORT_BY1" => "SORT",    // Поле для первой сортировки новостей
        "SORT_BY2" => "SORT",   // Поле для второй сортировки новостей
        "SORT_ORDER1" => "ASC",    // Направление для первой сортировки новостей
        "SORT_ORDER2" => "ASC", // Направление для второй сортировки новостей
        "STRICT_SECTION_CHECK" => "N",  // Строгая проверка раздела для показа списка
    ),
    false
);?>



<div class="tei">   

        

    <div class="tei-info">
        <div class="container">
            <img src="<?=SITE_TEMPLATE_PATH?>/ico/TEI_logo.svg" alt="TEI">
            <div class="tei_text">
	            <p>
	            Talent Equity Institute - внутренний исследовательский институт компании Ward Howell. Институт был основан в 2009 году и сейчас возглавляется Старшим партнером Ward Howell, профессором INSEAD Станиславом Шекшней. Основная задача института – создание и распространение новых знаний, инструментов и концептуальных моделей в области управленческих талантов и лидерства.
	            </p>
                    	<!-- <ul>
			<li>создание и распространение новых знаний об управленческих талантах и лидерстве на развивающихся рынках;</li>
			<li>разработку концептуальных моделей, а также консалтинговых и управленческих инструментов для развивающихся рынков;</li>
			<li>проведение исследований для компаний-клиентов.</li>
		</ul> -->
	</div>
        </div>
    </div>
    <div class="tei-label">
        <div class="container">
            <div class="tei-label__wrap">
                <h3>Исследуйте идеи по темам</h3>
                 <?$APPLICATION->IncludeComponent(
	"bitrix:search.tags.cloud", 
	"tegs_list", 
	array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "N",
		"COLOR_NEW" => "3E74E6",
		"COLOR_OLD" => "C0C0C0",
		"COLOR_TYPE" => "Y",
		"FILTER_NAME" => "",
		"FONT_MAX" => "15",
		"FONT_MIN" => "15",
		"PAGE_ELEMENTS" => "25",
		"PERIOD" => "",
		"PERIOD_NEW_TAGS" => "",
		"SHOW_CHAIN" => "Y",
		"SORT" => "NAME",
		"TAGS_INHERIT" => "N",
		"URL_SEARCH" => "/teinstitute/index.php",
		"WIDTH" => "100%",
		"arrFILTER" => array(
			0 => "iblock_news",
		),
		"arrFILTER_iblock_news" => array(
			0 => "1",
		),
		"COMPONENT_TEMPLATE" => "tegs_list"
	),
	false
);?>                 
                        </div>
                </div>
            </div>




<div class="article-search">
                <div class="container">
                    <div class="article-search__input">

                    <?$APPLICATION->IncludeComponent(
			"bitrix:search.form",
			"search_tei_form",
			Array(
				"PAGE" => "#SITE_DIR#search/tei.php",
				"USE_SUGGEST" => "N"
			)
		);?>
                        
                    </div>
<div class="js-tei-articles">
<?
$GLOBALS['arrFilter'] = array("TAGS" => '%'.$_GET['tags'].'%');
$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"tei_list", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "NAME",
			1 => "TAGS",
			2 => "PREVIEW_TEXT",
			3 => "PREVIEW_PICTURE",
			4 => "",
		),
		"FILTER_NAME" => "arrFilter",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "1",
		"IBLOCK_TYPE" => "Catalog",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "6",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "more",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "130",
		"PROPERTY_CODE" => array(
			0 => "VIDEO",
			1 => "TYPE",
			2 => "IMAGE_COVER",
			3 => "",
		),
		"SET_BROWSER_TITLE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"COMPONENT_TEMPLATE" => "tei_list"
	),
	false
);?>
</div>
</div>
</div>

<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"journal_list_scroll", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "CODE",
			1 => "XML_ID",
			2 => "NAME",
			3 => "TAGS",
			4 => "SORT",
			5 => "PREVIEW_TEXT",
			6 => "PREVIEW_PICTURE",
			7 => "DETAIL_TEXT",
			8 => "DETAIL_PICTURE",
			9 => "DATE_ACTIVE_FROM",
			10 => "ACTIVE_FROM",
			11 => "DATE_ACTIVE_TO",
			12 => "ACTIVE_TO",
			13 => "SHOW_COUNTER",
			14 => "SHOW_COUNTER_START",
			15 => "IBLOCK_TYPE_ID",
			16 => "IBLOCK_ID",
			17 => "IBLOCK_CODE",
			18 => "IBLOCK_NAME",
			19 => "IBLOCK_EXTERNAL_ID",
			20 => "DATE_CREATE",
			21 => "CREATED_BY",
			22 => "CREATED_USER_NAME",
			23 => "TIMESTAMP_X",
			24 => "MODIFIED_BY",
			25 => "USER_NAME",
			26 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "3",
		"IBLOCK_TYPE" => "Catalog",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "10",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "95",
		"PROPERTY_CODE" => array(
			0 => "TEXT_1",
			1 => "CONCLUSION_TEXT",
			2 => "VIDEO",
			3 => "TYPE",
			4 => "TEXT_HEAD_1",
			5 => "QUOTE",
			6 => "AUTHOR_QUOTE",
			7 => "AUTHOR_POSITION",
			8 => "LISTS",
			9 => "TEXT_2",
			10 => "QUOTE_2_HEAD",
			11 => "QUOTE_2",
			12 => "TEXT_HEAD_3",
			13 => "TEXT_3",
			14 => "TASKS",
			15 => "RESULTS_TASK",
			16 => "PERIOD",
			17 => "PRICE",
			18 => "CONCLUSION_NUMBER",
			19 => "CONCLUSION_HEAD",
			20 => "PDF_JOURNAL",
			21 => "IMAGE_COVER",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"COMPONENT_TEMPLATE" => "journal_list_scroll"
	),
	false
);?>

        </div>
        </div>
        </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>