<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Title");
?>
<div class="material event block-page">
<?$APPLICATION->IncludeComponent(
	"bitrix:news.detail", 
	"event", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_ELEMENT_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"BROWSER_TITLE" => "-",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "event",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_CODE" => $_REQUEST["CODE"],
		"ELEMENT_ID" => "",
		"FIELD_CODE" => array(
			0 => "NAME",
			1 => "TAGS",
			2 => "DETAIL_TEXT",
			3 => "DETAIL_PICTURE",
			4 => "DATE_CREATE",
			5 => "",
		),
		"IBLOCK_ID" => "9",
		"IBLOCK_TYPE" => "Catalog",
		"IBLOCK_URL" => "",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"MESSAGE_404" => "",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Страница",
		"PROPERTY_CODE" => array(
			0 => "TYPE",
			1 => "TEXT_HEAD_1",
			2 => "TEXT_1",
			3 => "QUOTE",
			4 => "AUTHOR_QUOTE",
			5 => "AUTHOR_POSITION",
			6 => "LISTS",
			7 => "TEXT_2",
			8 => "QUOTE_2_HEAD",
			9 => "QUOTE_2",
			10 => "TEXT_HEAD_3",
			11 => "TEXT_3",
			12 => "TASKS",
			13 => "RESULTS_TASK",
			14 => "PERIOD",
			15 => "PRICE",
			16 => "CONCLUSION_NUMBER",
			17 => "CONCLUSION_HEAD",
			18 => "CONCLUSION_TEXT",
			19 => "TEXT_4",
			20 => "DATE",
			21 => "TIME_START",
			22 => "TIME_END",
			23 => "ADDRESS",
			24 => "VIDEO",
			25 => "Text_4",
			26 => "IMAGE_ARTICLE",
			27 => "IMAGE_COVER",
			28 => "AUTHORS",
			29 => "JOURNAL",
			30 => "",
		),
		"SET_BROWSER_TITLE" => "Y",
		"SET_CANONICAL_URL" => "N",
		"SET_LAST_MODIFIED" => "Y",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"STRICT_SECTION_CHECK" => "N",
		"USE_PERMISSIONS" => "N",
		"USE_SHARE" => "N"
	),
	false
);?>




</div>
<div class="regevent">
            <div class="container">
                <div class="regevent__wrap">
                    <h3>Регистрация на мероприятие</h3>
                    <form class="regevent__form">
                        <input type="text" placeholder="Как к Вам обращаться?">
                        <input type="text" placeholder="Email">
                        <input type="text" placeholder="Телефон">
                        <button type="submit" class="btn"><span>Отправить заявку</span>
                        </button>
                        <div class="block-check">
                            <input type="checkbox" name="checkbox-form" id="checkbox-mail-regevent">
                            <label for="checkbox-mail-regevent" class="block-check__box"></label>
                            <label for="checkbox-mail-regevent" class="block-check__text">Согласен с <a href="#awe4235r">политикой конфиденциальности</a> сайта</label>
                        </div>
                    </form>
                </div>
            </div>
</div>
        
 <?
 $APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "news_main",
    Array(
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "ADD_SECTIONS_CHAIN" => "Y",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "DISPLAY_TOP_PAGER" => "N",
        "FIELD_CODE" => array("CODE","XML_ID","NAME","TAGS","SORT","PREVIEW_TEXT","PREVIEW_PICTURE","DETAIL_TEXT","DETAIL_PICTURE","DATE_ACTIVE_FROM","ACTIVE_FROM","DATE_ACTIVE_TO","ACTIVE_TO","SHOW_COUNTER","SHOW_COUNTER_START","IBLOCK_TYPE_ID","IBLOCK_ID","IBLOCK_CODE","IBLOCK_NAME","IBLOCK_EXTERNAL_ID","DATE_CREATE","CREATED_BY","CREATED_USER_NAME","TIMESTAMP_X","MODIFIED_BY","USER_NAME",""),
        "FILTER_NAME" => "",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "IBLOCK_ID" => "1",
        "IBLOCK_TYPE" => "news",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
        "INCLUDE_SUBSECTIONS" => "Y",
        "MESSAGE_404" => "",
        "NEWS_COUNT" => "10",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => ".default",
        "PAGER_TITLE" => "Новости",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "PREVIEW_TRUNCATE_LEN" => "95",
        "PROPERTY_CODE" => array("TYPE","TEXT_HEAD_1","TEXT_1","QUOTE","AUTHOR_QUOTE","AUTHOR_POSITION","LISTS","TEXT_2","QUOTE_2_HEAD","QUOTE_2","TEXT_HEAD_3","TEXT_3","TASKS","RESULTS_TASK","PERIOD","PRICE","CONCLUSION_NUMBER","CONCLUSION_HEAD","CONCLUSION_TEXT","TEXT_4","VIDEO",""),
        "SET_BROWSER_TITLE" => "Y",
        "SET_LAST_MODIFIED" => "N",
        "SET_META_DESCRIPTION" => "Y",
        "SET_META_KEYWORDS" => "Y",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "Y",
        "SHOW_404" => "N",
        "SORT_BY1" => "ACTIVE_FROM",
        "SORT_BY2" => "SORT",
        "SORT_ORDER1" => "DESC",
        "SORT_ORDER2" => "ASC",
        "STRICT_SECTION_CHECK" => "N"
    )
);?>


        <div class="subscribe">
            <div class="container">
                <div class="subscribe__wrap">

                       <?$APPLICATION->IncludeFile(
		            $APPLICATION->GetTemplatePath("/include/html/contact_us.php"),
		            Array(),
		            Array("MODE"=>"html")
		            );
		?>         
		<?$APPLICATION->IncludeComponent(
			"ward:subscribe",
			"form",
			array(),
			false
		);?>


                </div>
            </div>
        </div>




<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>