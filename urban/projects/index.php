<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Проекты");
?>
<div class="body-page-padding">
	<div class="section-grey">
		<div class="container">
			<div class="page-catalog-filter">
				<div class="page-catalog__header">
					<h1>КАТАЛОГ ГОТОВЫХ ПРОЕКТОВ</h1>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="b-catalog-filter">
			 <?
			 $APPLICATION->IncludeComponent("bitrix:catalog.smart.filter", "filter2", Array(
				"CACHE_GROUPS" => "Y",	// Учитывать права доступа
					"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
					"CACHE_TYPE" => "A",	// Тип кеширования
					"DISPLAY_ELEMENT_COUNT" => "Y",	// Показывать количество
					"FILTER_NAME" => "arrFilter",	// Имя выходящего массива для фильтрации
					"FILTER_VIEW_MODE" => "horizontal",	// Вид отображения
					"IBLOCK_ID" => "1",	// Инфоблок
					"IBLOCK_TYPE" => "products",	// Тип инфоблока
					"PAGER_PARAMS_NAME" => "arrPager",	// Имя массива с переменными для построения ссылок в постраничной навигации
					"POPUP_POSITION" => "left",
					"PREFILTER_NAME" => "smartPreFilter",	// Имя входящего массива для дополнительной фильтрации элементов
					"SAVE_IN_SESSION" => "N",	// Сохранять установки фильтра в сессии пользователя
					"SECTION_CODE" => "",	// Код раздела
					"SECTION_CODE_PATH" => "",	// Путь из символьных кодов раздела
					"SECTION_DESCRIPTION" => "-",	// Описание
					"SECTION_ID" => $_REQUEST["SECTION_ID"],	// ID раздела инфоблока
					"SECTION_TITLE" => "-",	// Заголовок
					"SEF_MODE" => "Y",	// Включить поддержку ЧПУ
					"SEF_RULE" => "",	// Правило для обработки
					"SMART_FILTER_PATH" => "",	// Блок ЧПУ умного фильтра
					"TEMPLATE_THEME" => "blue",	// Цветовая тема
					"XML_EXPORT" => "N",	// Включить поддержку Яндекс Островов
				),
				false
			);
			?>
			</div>
		</div>	
	</div>

<?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"table1",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array("",""),
		"FILTER_NAME" => "arrFilter",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "1",
		"IBLOCK_TYPE" => "products",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "30",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
//		"PAGER_TEMPLATE" => ".default",
//		"PAGER_TEMPLATE" => "bootstrap_v4",
//		"PAGER_TEMPLATE" => "grid",
//		"PAGER_TEMPLATE" => "modern",
//		"PAGER_TEMPLATE" => "orange",
		"PAGER_TEMPLATE" => "round",
//		"PAGER_TEMPLATE" => "visual",
		"PAGER_TITLE" => "Проекты",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array("PRICE_BASE","TOTAL_AREA","PHOTOS",""),
		"SET_BROWSER_TITLE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"SORT_BY1" => "NAME",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N"
	)
);?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>