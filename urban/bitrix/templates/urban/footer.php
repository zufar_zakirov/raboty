

</div>
<!-- подвал -->
		<section class="footer ">
			<div class="container">
				<div class="row">
					
					<div class="col-12 col-lg-6">
						<div class="footer-logo">
							<img src="/img/logo.png?v=2" class="">
						</div>
<!-- 						<div class="copyright">
							© 2003-2020 ИП ГЛУХОВА ЭЛЬВИРА ЛЕОНИДОВНА
						</div> -->
						<div class="footer-text mt-3">
							Проекты домов и коттеджей.
							<br />
							Экспертиза и технический надзор.
							<br />
							<a href="/company/privacy-policy.php">Политика конфиденциальности</a>
							<br />
							<a href="/company/condition-use.php">Условия пользования сайтом</a>
						</div>
					</div>

					<div class="col-12 col-lg-6">
						<div class="b-footer-contacts">
							<div class="container">
								<div class="row">
									<div class="col-1">
										<i class="ico-contact-address"></i>
									</div>
									<div class="col-10">
										<div class="contact-address contact-text">426000, Удмуртская Республика, г. Ижевск, ул. Коммунаров, д.224Д, оф.203</div>
									</div>
								</div>

								<div class="row pt-2">
									<div class="col-1">
										<i class="ico-contact-phone"></i>
									</div>
									<div class="col-10">
										<div class="contact-phone contact-text phone_alloka">8 800 300 57 84<div class="small header-small">(звонок бесплатный)</div></div>
									</div>
								</div>

								<div class="row pt-2">
									<div class="col-1">
										<i class="ico-contact-phone"></i>
									</div>
									<div class="col-10">
										<div class="contact-phone contact-text">8 958 844 01 46<div class="small header-small">(whatsapp, viber, telegram)</div>
										</div>
									</div>
								</div>

								<div class="row pt-2">
									<div class="col-1">
										<i class="ico-contact-email"></i>
									</div>
									<div class="col-10">
										<div class="contact-email contact-text js-module-write-us" data-fancybox data-src="#form-write-us">написать нам</div>
									</div>
								</div>

								<div class="row pt-2">
									<div class="col-1">
										<a href="https://vk.com/urbancompany">
											<i class="ico-contact-vk"></i>
										</a>
									</div>
									<div class="col-10">
										<a class="contact-email contact-text" href="https://vk.com/urbancompany">Мы в ВКонтакте</a>
									</div>
								</div>
							</div>

							<!-- <div class="contact-address">426000, Удмуртская Республика, г. Ижевск, ул. Коммунаторов, д.224Д, оф.208</div>
							<div class="contact-phone">+7 958 844 01 46</div>
							<div class="contact-email">info@urbancompany.ru</div>
							<div class="contact-vk">Мы в VK</div> -->
						</div>
					</div>

				</div>				
			</div>			
		</section>
<!-- подвал -->


	</div>







	<div class="module module-write_review">
		<div class="module-form">
			<div class="close js-close">X</div>
			<div class="header">
				<h3>ОСТАВИТЬ ОТЗЫВ</h3>
			</div>
			<div class="form js-inputs">
				<form action="" method="post">
					<div class="form-row">
						<input type="hidden" value="Оставить отзыв" name="header" class="js-input" />
						
						<input type="text" name="name" placeholder="Имя" class="js-input" required="required">
					</div>
					
					<div class="form-row">
						<textarea class="js-input" name="review" required="required"></textarea>
					</div>
					<div class="form-row">
						<input type="button" name="button" class="button-modal send_review js-submit" value="НАПИСАТЬ">
					</div>
				</form>
			</div>			
		</div>
	</div>


	<div class="module module-write-us" id='form-write-us'>
		<div class="module-form">
			<!-- <div class="close js-close">X</div> -->
			<div class="header">
				<h3>Написать нам</h3>
			</div>
			<div class="form js-inputs">
				<form action="" method="post" id="form-2">
					<div class="form-row">
						<input type="hidden" value="Написать нам" name="header" class="js-input" />
						
						<input type="text" name="name" placeholder="Имя*" class="js-input" required="required" id="name-2">
					</div>
					<div class="form-row">
						<input type="text" name="phone" placeholder="Телефон*" class="js-input" required="required" id="phone-2">
					</div>
					<div class="form-row">
						<input type="text" name="email" placeholder="Email" class="js-input">
					</div>
					<div class="form-row">
						<input type="text" name="city" placeholder="Город" class="js-input">
					</div>
					
					<div class="form-row">
						<textarea class="js-input" name="text" placeholder="Сообщение"></textarea>
					</div>
					<div class="form-row">
						<input type="button" name="button" 
						class="button-modal send_review js-submit"
						js-location="/res/res.php?f=footer"
						value="НАПИСАТЬ">
					</div>
				</form>
			</div>
			<div class="text-small">
				Если Вам не перезвонили в течении 30 минут, наберите нас и мы предоставим Вам скидку.
			</div>
		</div>
	</div>





	<div class="module module-call-back" id='form-call-back'>
		<div class="module-form">
			<!-- <div class="close js-close">X</div> -->
			<div class="header">
				<h3>Обратный звонок</h3>				
			</div>
			<div class="form">
				<form action="#" method="post" class="js-inputs" id="form-1">
					<div class="form-row">
						<input type="hidden" value="Обратный звонок" name="header" class="js-input" />
						<input type="text" name="name" placeholder="Имя*" class="js-input" required="required" id="name-1">
					</div>
					<div class="form-row">
						<input type="text" name="phone" placeholder="Телефон*" class="js-input" required="required" id="phone-1">
					</div>
					<div class="form-row">
						<input type="text" name="city" placeholder="Город" class="js-input" id="city-1">
					</div>					
					<div class="form-row">
						<input type="button" name="button" class="button-modal send_review js-submit" 
						js-location="/res/res.php?f=callback"
						value="ОТПРАВИТЬ">
					</div>
				</form>
			</div>
			<div class="text-small">
				Если Вам не перезвонили в течении 30 минут, наберите нас и мы предоставим Вам скидку.
			</div>
		</div>
	</div>


<div class="hide cookie-notify js-cookie-accept">
<div class="cookie-notify__content">Сайт использует файлы cookies и сервис сбора технических данных его посетителей. Продолжая использовать данный ресурс, вы автоматически соглашаетесь с использованием данных технологий. <button class="btn btn-primary btn-sm"><span>OK</span></button></div>
</div>




<div class="phone-fixed-right b-model-icon js-module-call-back b-phone b-model-icon" data-fancybox data-src="#form-call-back">
	<i class="icon-phone-waves"></i>
    <span class="modal-btn-text">Обратный звонок</span>
</div>

<script
src="https://code.jquery.com/jquery-3.5.1.min.js?v=2"
integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
crossorigin="anonymous"></script>

  
<script type="text/javascript" src="/bitrix/templates/urban/js/slick.min.js"></script>
<script type="text/javascript" src="/bitrix/templates/urban/js/js.cookie.js"></script>
<script type="text/javascript" src="/bitrix/templates/urban/js/js.js?v=7"></script>

<script src="/bitrix/templates/urban/js/jquery.fancybox.min.js"></script>

<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script> -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.5/jquery.inputmask.min.js"></script>
  

<script type="text/javascript">
$(document).ready(function(){

  // $('.b-reviews--list').slick({
  //   	dots: false,
  //       infinite: true,
  //       slidesToShow: 4,
  //       slidesToScroll: 1,

  //       responsive: [
	 //    	{
		//     breakpoint: 865,
		//     settings: {
		//     		arrows: true,
		// 	    	slidesToShow: 2,
		// 	    	slidesToScroll: 2
	 //     		}
	 //     	},
  //    		{
	 //      	breakpoint: 480,
	 //      	settings: {
		//         	arrows: true,
		//         	// centerMode: true,
		//         	centerPadding: '20px',
		//         	slidesToShow: 1
  //     			}
  //     		}
  //   	] 
  // });



  $('.thumbnail-list').slick({
    	dots: false,
        infinite: false,
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.main-image',
        focusOnSelect: true,

        responsive: [
	    	{
		    breakpoint: 865,
		    settings: {
		    		infinite: false,
		    		arrows: true,
			    	slidesToShow: 2,
			    	slidesToScroll: 2
	     		}
	     	},
     		{
	      	breakpoint: 480,
	      	settings: {
	      			infinite: false,
		        	arrows: true,
		        	// centerMode: true,
		        	centerPadding: '20px',
		        	slidesToShow: 2
      			}
      		}
    	] 
  });


	$('.main-image').slick({arrows:false,infinite:false,asNavFor:'.thumbnail-list',});


	$('.headslider').slick({
		dots: true,
		autoplay:false,
  		arrows:false,
	    infinite: true,
	    slidesToShow: 1,
	    slidesToScroll: 1,
	});
});
</script>
<script type="text/javascript">
(function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
(window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

ym(66829480, "init", {
clickmap:true,
trackLinks:true,
accurateTrackBounce:true,
webvisor:true
});
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/66829480" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!— Global site tag (gtag.js) - Google Analytics —>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-177201750-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-177201750-1');
</script>
</body>
</html>