<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);

if (empty($arResult["ALL_ITEMS"]))
	return;

CUtil::InitJSCore();

$menuBlockId = "catalog_menu_".$this->randString();
?>
<ul class="menu-top--list" id="ul_<?=$menuBlockId?>">
		<?
		foreach($arResult["MENU_STRUCTURE"] as $itemID => $arColumns)
		{
		    //--first level--
			$existPictureDescColomn = ($arResult["ALL_ITEMS"][$itemID]["PARAMS"]["picture_src"] || $arResult["ALL_ITEMS"][$itemID]["PARAMS"]["description"]) ? true : false;
			$class = "";
			$class_link = '';
			if($arResult["ALL_ITEMS"][$itemID]["SELECTED"])
			{
				$class .= " bx-active";
			}
			if(is_array($arColumns) && count($arColumns) > 0)
			{
				$class .= " parent";
				$class_link = ' js-li-parent link-parent';
			}
		?>
			<li
				class="<?=$class?>"				
				<?if (is_array($arColumns) && count($arColumns) > 0):?>
					data-role="bx-menu-item"
					onclick="if (BX.hasClass(document.documentElement, 'bx-touch')) obj_<?=$menuBlockId?>.clickInMobile(this, event);"
				<?endif?>
			>
				<a
					class="<?=$class_link?>"
					href="<?=$arResult["ALL_ITEMS"][$itemID]["LINK"]?>"
					<?if (is_array($arColumns) && count($arColumns) > 0 && $existPictureDescColomn):?>
						onmouseover="window.obj_<?=$menuBlockId?> && obj_<?=$menuBlockId?>.changeSectionPicure(this, '<?=$itemID?>');"
					<?endif?>
				>					
					<?=htmlspecialcharsbx($arResult["ALL_ITEMS"][$itemID]["TEXT"], ENT_COMPAT, false)?>
				</a>
				<?
				if (is_array($arColumns) && count($arColumns) > 0)
				{
				
					foreach($arColumns as $key=>$arRow)
					{
					?>
					<ul class="child-menu">
					<?foreach($arRow as $itemIdLevel_2=>$arLevel_3):?>  <!-- second level-->
						<li class="bx-nav-2-lvl">
							<a
								href="<?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["LINK"]?>"
								>
								<?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["TEXT"]?>
								</a>
						</li>
					<?endforeach;?>
					</ul>
					<?
				}
			}
			?>
		</li>
	<?
	}
	?>
</ul>

