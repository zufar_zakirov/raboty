<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


function tpl_wordForm($n, $form1 = 'комната', $form2='комнаты', $form3='комнат')
{
    $n = abs($n) % 100;
    $n1 = $n % 10;

    if ($n > 10 && $n < 20) {
      return $form3;
    }

    if ($n1 > 1 && $n1 < 5) {
      return $form2;
    }

    if ($n1 == 1) {
    return $form1;
    }

    return $form3;
}

function round_hundred($number){
    return round( $number/100 ) * 100;
}

$value = $arResult["PROPERTIES"]["COUNT_ROOMS"]['VALUE'];
$arResult["PROPERTIES"]["COUNT_ROOMS"]['word_room'] = tpl_wordForm($value);

$arResult["PROPERTIES"]['price1'] = round_hundred( (30*$arResult["PROPERTIES"]['PRICE_BASE']['VALUE'])/100 );
$arResult["PROPERTIES"]['price2'] = round_hundred( (30*$arResult["PROPERTIES"]['PRICE_BASE']['VALUE'])/100+3000 );
$arResult["PROPERTIES"]['price3'] = round_hundred( (50*$arResult["PROPERTIES"]['PRICE_BASE']['VALUE'])/100 );
$arResult["PROPERTIES"]['price4'] = round_hundred( (100*$arResult["PROPERTIES"]['PRICE_BASE']['VALUE'])/100 );


function getContentProject(&$arResult)
{
    $arSort = array('SORT' => 'ASC', 'ID' => 'DESC');
    $arFilter = array('ACTIVE' => 'Y', 'IBLOCK_ID' => '11');
    $arSelect = array('ID', 'NAME', 'DETAIL_TEXT');

    $res = CIBlockElement::getList($arSort, $arFilter, false, false, $arSelect);

    while($ob = $res->GetNextElement()){ 
        $arResult['content_project'][] = $ob->GetFields();        
    }
}

function getDopUslugi(&$arResult)
{
    $arSort = array('SORT' => 'ASC', 'ID' => 'DESC');
    $arFilter = array('ACTIVE' => 'Y', 'IBLOCK_ID' => '12');
    $arSelect = array('ID', 'NAME', 'DETAIL_TEXT');

    $res = CIBlockElement::getList($arSort, $arFilter, false, false, $arSelect);

    while($ob = $res->GetNextElement()){ 
        $arResult['dop_uslugi'][] = $ob->GetFields();        
    }
}



if (\Bitrix\Main\Loader::includeModule('iblock')) {
    getContentProject($arResult);
    getDopUslugi($arResult);
}