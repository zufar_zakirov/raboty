<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$arProp = $arResult["PROPERTIES"]; 


$img_main = CFile::ResizeImageGet( $arProp["PHOTOS"]["VALUE"][0]
    , Array("width" => 648, "height" => 572), BX_RESIZE_IMAGE_EXACT, false);

$img_main_big = CFile::ResizeImageGet( $arProp["PHOTOS"]["VALUE"][0]
    , Array("width" => 1920, "height" => 1080), BX_RESIZE_IMAGE_EXACT, false);


$arImg = array();
foreach ($arProp["PHOTOS"]["VALUE"] as $key => $value) {


	$arImg[$key]['big'] = CFile::ResizeImageGet( $value
    , Array("width" => 1920, "height" => 1080), BX_RESIZE_IMAGE_EXACT, false);

	$arImg[$key]['middle'] = CFile::ResizeImageGet( $value
    , Array("width" => 648, "height" => 572), BX_RESIZE_IMAGE_EXACT, false);

    $arImg[$key]['thumb'] = CFile::ResizeImageGet( $value
    , Array("width" => 134, "height" => 132), BX_RESIZE_IMAGE_EXACT, false);
}



$arPlans = array();
foreach ($arProp["PLANS"]["VALUE"] as $key => $value) {

	$arPlans[$key]['big'] = CFile::ResizeImageGet( $value
    , Array("width" => 1920, "height" => 1080), BX_RESIZE_IMAGE_EXACT, false);

    $arPlans[$key]['thumb'] = CFile::ResizeImageGet( $value
    , Array("width" => 483, "height" => 230), BX_RESIZE_IMAGE_EXACT, false);

    $arPlans[$key]['desc'] = $arProp["PLANS"]["DESCRIPTION"][$key];
}



$arFacades = array();
foreach ($arProp["FACADE"]["VALUE"] as $key => $value) {

	$arFacades[$key]['big'] = CFile::ResizeImageGet( $value
    , Array("width" => 1920, "height" => 1080), BX_RESIZE_IMAGE_EXACT, false);

    $arFacades[$key]['thumb'] = CFile::ResizeImageGet( $value
    , Array("width" => 483, "height" => 230), BX_RESIZE_IMAGE_EXACT, false);

    $arFacades[$key]['desc'] = $arProp["FACADE"]["DESCRIPTION"][$key];
}


?>
<div class="section section-grey">
		<div class="container">
			<div class="item">
				<div class="item__header">
					<h1>ПРОЕКТ ДОМА <span class="text-orange"><?=$arResult["NAME"]?></span></h1>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row-detail-block">
				<div class="cols-detail-1">
					<div class="colomn-detail-images">
						<div class="main-image">
							<!-- <a href="<?//=$img_main_big['src']?>" data-fancybox="images" class="lightbox-link">
								<img src="<?//=$img_main['src']?>" class="main-img" />
							</a> -->
							<?foreach($arImg as $key => $val ):?>
								<?if(0 == $key){
									//continue;
								}?>
								<a href="<?=$val['big']['src']?>" data-fancybox="images" class="lightbox-link">
									<img src="<?=$val['middle']['src']?>" class="main-img" />
								</a>
							<?endforeach;?>	
						</div>
						<div class="thumbnail-scroll carousel" id="carousel">
							<!-- <button class="js-prev btn-prev prev-img-item"></button> -->
							
							<div class="gallery">
								<div class="thumbnail-list">
									<?foreach($arImg as $key => $val ):?>
										<div class="b-thumbnail-item">										
											<img src="<?=$val['thumb']['src']?>" class="thumbnail-item" 
												attr-middle-img="<?=$arImg[$key]['middle']['src']?>"
												attr-big-img="<?=$arImg[$key]['big']['src']?>"
												data-class=".lightbox-link"
												data-key="<?=$key?>"
											/>											
										</div>
									<?endforeach;?>	
								</div>
							</div>

							<!-- <button class="js-next btn-next  next-img-item"></button> -->
						</div>
					</div>
				</div>


				<div class="cols-detail-2">
					<div class="colomn-detail-properties">
						
						<div class="properties--icon">
							<div class="row-properties--icon w-240">
								<div class="total-square property--icon">
									<div class="total-square-text">
										<?=$arProp["TOTAL_AREA"]['VALUE']?>
										<span class="text-sm">м</span><sup>2</sup>
									</div>
								</div>

							</div>

							<div class="row-properties--icon">
								<div class="icon-size-detail property--icon">
									<div class="size-house-text">
										<!-- 15<span class="text-sm">x</span>10 -->
										<?=$arProp["SIZE_HOUSE"]['VALUE']?>
										<span class="text-sm">м</span>
									</div>									
								</div>
							</div>
						</div>

						<div class="properties--icon">
							<div class="row-properties--icon w-240 pl-1">
								<div class="icon-rooms--detail property--icon">
									<div class="">
										<div class="line-height-sm">
											<?=$arProp["COUNT_ROOMS"]['VALUE']?>
											<br />
											<span class="rooms-text-detail"><?=$arProp["COUNT_ROOMS"]['word_room']?></span>
										</div>
									</div>
								</div>

							</div>

							<div class="row-properties--icon">
								<div class="icon-garage-detail property--icon garage-is">
									есть
								</div>
							</div>
						</div>

					</div>

					<div class="buttons-detail">
						<button class="question ask-question js-module-question" data-fancybox data-src="#form-question">ЗАДАТЬ ВОПРОС АРХИТЕКТОРУ</button>
						<button class="question get-plan js-module-get_plan" data-fancybox data-src="#form-get_plan">ПОЛУЧИТЬ ПЛАН С ТОЧНЫМИ РАЗМЕРАМИ</button>
						<button class="question save-cart js-module-save_card" data-fancybox data-src="#form-save_card">ПОЛУЧИТЬ КАРТОЧКУ ПРОЕКТА</button>
					</div>


					<div class="sections-price">
						<div class="row-sections-price">
							<div class="section-price-name">
								<span class="price-name-tarif">АР</span>
								<div class="section-price">
									Цена: <span class="price-bold"><?=$arProp['ALBUM_AR_1']['VALUE'] //=$arProp['price1'];?></span> руб.
								</div>
							</div>
							<div class="download_example">
								<a href="javascript:;" class="link_download_example js-module-download-example"
								js-data-name="АР"
								data-fancybox data-src="#form-download-example"
								>Скачать пример</a>
							</div>
						</div>

						<div class="row-sections-price">
							<div class="section-price-name">
								<span class="price-name-tarif">АР + схема привязки здания на участке</span>
								<div class="section-price">
									Цена: <span class="price-bold"><?=$arProp['ALBUM_AR_2']['VALUE']//['price2'];?></span> руб.
								</div>
							</div>
							<div class="download_example">
								<a href="" class="link_download_example js-module-download-example"
								js-data-name="АР + схема привязки здания на участке"
								data-fancybox data-src="#form-download-example"
								>Скачать пример</a>
							</div>
						</div>

						<div class="row-sections-price">
							<div class="section-price-name">
								<span class="price-name-tarif">АР + КР-1 (схема привязки здания на участке бесплатно)</span>
								<div class="section-price">
									Цена: <span class="price-bold"><?=$arProp['ALBUM_AR_3']['VALUE']//['price3'];?></span> руб.
								</div>
							</div>
							<div class="download_example">
								<a href="" class="link_download_example js-module-download-example"
								js-data-name="АР + КР-1 (схема привязки здания на участке бесплатно)"
								data-fancybox data-src="#form-download-example"
								>Скачать пример</a>
							</div>
						</div>

						<div class="row-sections-price">
							<div class="section-price-name">
								<span class="price-name-tarif">АР + КР-1 + КР-2 (схема привязки здания на участке бесплатно)</span>
								<div class="section-price">
									Цена: <span class="price-bold"><?=$arProp['ALBUM_AR_4']['VALUE']//['price4'];?></span> руб.
								</div>
							</div>
							<div class="download_example">
								<a href="" class="link_download_example js-module-download-example"
								js-data-name="АР + КР-1 + КР-2 (схема привязки здания на участке бесплатно)"
								data-fancybox data-src="#form-download-example"
								>Скачать пример</a>
							</div>
						</div>

					</div>

					<div class="b-button-order">
						<button class="button-order js-module-order_project" data-fancybox data-src="#form-order">ЗАКАЗАТЬ</button>
					</div>
				</div>
			</div>
		</div>
	</div>	




	<div class="section">
		<div class="container">
			<div class="tabs ">
				<div class="tab-item tab-1 selected" attr-block=".tab-info-1">
					<div class="tab-text js-tab-1">Планы и фасады</div>
				</div>
				<div class="tab-item tab-2 " attr-block=".tab-info-2">
					<div class="tab-text js-tab-2">Описание</div>
				</div>
				<div class="tab-item tab-3" attr-block=".tab-info-3">
					<div class="tab-text js-tab-3">Состав проекта</div>
				</div>
				<div class="tab-item tab-4" attr-block=".tab-info-4">
					<div class="tab-text js-tab-4">Дополнительные услуги</div>
				</div>
			</div>


			<div class="tabs-info">
				<h3 class="m-active tab-accordion mobile js-tab-mob" attr-block=".tab-info-1">Планы и фасады</h3>
				<div class="tab-info tab-info-1 active">
					<div class="tab-info-text plans-facade">
						
						<div class="plans-header">
							<h3>Планы</h3>
						</div>
						<div class="plans">

							<?foreach($arPlans as $key => $val):?>
								<div class="plan-item">
									<?if(isset($val['desc'])):?>
										<h5><?=$val['desc']?></h5>
									<?endif?>	
									<a href="<?=$val['big']['src']?>" class="" data-fancybox="images-2" data-title="<?=$val['desc']?>">
										<img src="<?=$val['thumb']['src']?>">
									</a>
								</div>
							<? endforeach;?>
						</div>
						

						<hr />

						<div class="plans-header">
							<h3>Фасады</h3>
						</div>
						<div class="facades">							
							<?foreach($arFacades as $key => $val):?>
								<div class="facade-item">
									<?if(!empty($val['desc'])):?>
										<h5><?=$val['desc']?></h5>
									<?endif?>	
									<a href="<?=$val['big']['src']?>" class="" data-fancybox="image-2" data-title="<?=$val['desc']?>">
										<img src="<?=$val['thumb']['src']?>">
									</a>
								</div>
							<?endforeach;?>
						</div>
						
					</div>
				</div>

				<h3 class="tab-accordion mobile js-tab-mob" attr-block=".tab-info-2">Описание</h3>
				<div class="tab-info tab-info-2 description">
					<div class="tab-info-text">
						<?=$arProp["DESCRIPTION"]["~VALUE"]["TEXT"]?>
					</div>
				</div>

				<h3 class="tab-accordion mobile js-tab-mob" attr-block=".tab-info-3">Состав проекта</h3>
				<div class="tab-info tab-info-3 sostav-project">
					<div class="tab-info-text">
						<div class="container">
							<div class="question-list">
								<? foreach ($arResult['content_project'] as $key => $value):?>
									<div class="question-item row-album">
										<div class="question-head">
											<?=$value['NAME']?>
										</div>
										<div class="answer-text">
											<?=$value['DETAIL_TEXT']?>
											<div class="b-button--in-dop-uslugi">
												<button class="question ask-question in-dop-uslugi 
												js-module-download-example"
												js-data-name="<?=$value['NAME']?>"
												data-fancybox data-src="#form-download-example"
												>
													СКАЧАТЬ ПРИМЕР
												</button>
											</div>
										</div>
									</div>
								<? endforeach; ?>								
							</div>
						</div>
					</div>
				</div>

				<h3 class="tab-accordion mobile js-tab-mob" attr-block=".tab-info-4">Дополнительные услуги</h3>
				<div class="tab-info tab-info-4">
					<div class="tab-info-text optional-services">

						<div class="container">
							<div class="question-list">
								<? foreach ($arResult['dop_uslugi'] as $key => $value):?>
									<div class="question-item row-album">
										<div class="question-head">
											<?=$value['NAME']?>
										</div>
										<div class="answer-text">
											<?=$value['DETAIL_TEXT']?>
											<div class="b-button--in-dop-uslugi">
												<button class="question ask-question in-dop-uslugi js-module-question" 
												data-fancybox data-src="#form-question">
													ЗАДАТЬ ВОПРОС АРХИТЕКТОРУ
												</button>
											</div>
										</div>
									</div>
								<? endforeach; ?>								
							</div>
						</div>
						



<!-- 						<div class="redevelopment-services optional-service-item">
							<div class="icon-services icon-optional-services"></div>
							<h4>ПЕРЕПЛАНИРОВКА</h4>
							<p>(поменять стены без изменений несущих конструкций)</p>
						</div>


						<div class="facade-services optional-service-item">
							<div class="icon-modification-facade icon-optional-services"></div>
							<h4>ИЗМЕНЕНИЕ ФАСАДА</h4>
						</div>

						<div class="facade-services optional-service-item">
							<div class="icon-replace-facade icon-optional-services"></div>
							<h4>ЗАМЕНА МАТЕРИАЛОВ</h4>
						</div>

						<div class="blueprints-services optional-service-item">
							<div class="icon-blueprints icon-optional-services"></div>
							<h4>ДОПОЛНИТЕЛЬНЫЙ КОМПЛЕКТ ЧЕРТЕЖЕЙ В БУМАЖНОВ ВИДЕ</h4>
							<p>(свыше 2 экз.)</p>
						</div>


						<div class="consultant-services optional-service-item">
							<div class="icon-consultant icon-optional-services"></div>
							<h4>ДОПОЛНИТЕЛЬНЫЙ КОМПЛЕКТ ЧЕРТЕЖЕЙ В БУМАЖНОВ ВИДЕ</h4>
							<p>(свыше 2 экз.)</p>
						</div>


						<div class="modification-house-services optional-service-item">
							<div class="icon-modification-house icon-optional-services"></div>
							<h4>ДОРАБОТКА / ПРОЕКТИРОВАНИЕ СОПУСТВУЮЩИХ ОБЪЕКТОВ К ГОТОВОМУ ПРОЕКТУ</h4>
							<p>(баня, гараж, беседка...)</p>
						</div> -->

					</div>
				</div>
			</div>
		</div>
	</div>	
</div>


<div class="module module-order_project" id="form-order">
	<div class="module-form">
		<!-- <div class="close js-close">X</div> -->
		<div class="header">
			<h3>ЗАКАЗАТЬ ПРОЕКТ</h3>
		</div>
		<div class="form">
			<form action="" method="post" class="js-inputs" id="form-3">
				<div class="form-row">
					<input type="hidden" name="header" value="Заказать" class="js-input" />
					<input type="text" name="name" placeholder="Имя*" class="js-input" id="name-3" required="required" >
				</div>
				<div class="form-row">
					<input type="text" name="phone" placeholder="Телефон*" class="js-input" id="phone-3" required="required" >
				</div>
				<div class="form-row">
					<input type="email" name="email" placeholder="Email" class="js-input">

					<input type="hidden" name="project" value="<?=$arResult["NAME"]?>" class="js-input" placeholder="Проект" />

					<input type="hidden" name="project-link" 
					value="http://urbancompany.ru/projects/<?=$arResult["NAME"]?>" class="js-input" placeholder="Ссылка" />
				</div>
				<div class="form-row">
					<input type="text" name="city" placeholder="Город" class="js-input">
				</div>
				<div class="form-row">
					<input type="button" name="button" class="button-modal js-submit"
					js-location="/res/res.php?f=order"
					value="ЗАКАЗАТЬ">
				</div>
			</form>
		</div>
		<div class="text-small">
			Если Вам не перезвонили в течении 30 минут, наберите нас и мы предоставим Вам скидку.
		</div>
	</div>
</div>


<div class="module module-save_card" id='form-save_card'>
	<div class="module-form">
		<!-- <div class="close js-close">X</div> -->
		<div class="header">
			<h3>ПОЛУЧИТЬ КАРТОЧКУ ПРОЕКТА</h3>
			<p>
				*Карточку вышлем на email.
			</p>
		</div>
		<div class="form">
			<form action="" method="post" class="js-inputs" id="form-4">
				<div class="form-row">
					<input type="hidden" name="header" class="js-input" value="Получить карточку проекта">
					<input type="text" name="name" placeholder="Имя*" class="js-input" required="required" id="name-4">
				</div>
				<div class="form-row">
					<input type="text" name="phone" placeholder="Телефон*" class="js-input" required="required" id="phone-4">
				</div>
				<div class="form-row">
					<input type="email" name="email" placeholder="Email" class="js-input">

					<input type="hidden" name="project" value="<?=$arResult["NAME"]?>" class="js-input" placeholder="Проект" />

					<input type="hidden" name="project-link" 
					value="http://urbancompany.ru/projects/<?=$arResult["NAME"]?>" class="js-input" placeholder="Ссылка" />
				</div>
				<div class="form-row">
					<input type="text" name="city" placeholder="Город" class="js-input">
				</div>
				<div class="form-row">
					<input type="button" name="button" class="button-modal js-submit" 
					js-location="/res/res.php?f=send_card"
					value="ВЫСЛАТь КАРТОЧКУ">
				</div>

			</form>
		</div>
		<div class="text-small">
			Если Вам не перезвонили в течении 30 минут, наберите нас и мы предоставим Вам скидку.
		</div>	
	</div>
</div>


<div class="module module-get_plan" id='form-get_plan'>
	<div class="module-form">
		<!-- <div class="close js-close">X</div> -->
		<div class="header">
			<h3>ПОЛУЧИТЬ ПЛАН С ТОЧНЫМИ РАЗМЕРАМИ</h3>
			<p>
				*План вышлем на email.
			</p>
		</div>
		<div class="form">
			<form action="" method="post" class="js-inputs" id="form-5">
				<div class="form-row">
					<input type="hidden" value="Получить план с точными размерами" 
						name="header" class="js-input" />
					<input type="text" name="name" placeholder="Имя*" class="js-input" id="name-5" required="required" />
				</div>
				<div class="form-row">
					<input type="text" name="phone" placeholder="Телефон*" class="js-input" id="phone-5" required="required" />
				</div>
				<div class="form-row">

					<input type="hidden" name="project" value="<?=$arResult["NAME"]?>" class="js-input" placeholder="Проект" />

					<input type="hidden" name="project-link" 
					value="http://urbancompany.ru/projects/<?=$arResult["NAME"]?>" class="js-input" placeholder="Ссылка" />

					<input type="email" name="email" placeholder="Email" class="js-input">
				</div>
				<div class="form-row">
					<input type="text" name="city" placeholder="Город" class="js-input">
				</div>
				<div class="form-row">
					<input type="button" name="button" class="button-modal js-submit" 
					js-location="/res/res.php?f=send_plan"
					value="ВЫСЛАТЬ ПЛАН">
				</div>

			</form>
		</div>
		<div class="text-small">
			Если Вам не перезвонили в течении 30 минут, наберите нас и мы предоставим Вам скидку.
		</div>	
	</div>
</div>


<div class="module module-question" id='form-question'>
	<div class="module-form">
		<!-- <div class="close js-close">X</div> -->
		<div class="header">
			<h3>ЗАДАТЬ ВОПРОС АРХИТЕКТОРУ</h3>
		</div>
		<div class="form">
			<form class="js-inputs" action="#" method="post" id="form-6">
				<div class="form-row">
					<input type="hidden" value="Задать вопрос архитектору" name="header" class="js-input" />						
					<input type="text" name="name" placeholder="Имя*" class="js-input" id="name-6" required="required" />
				</div>
				<div class="form-row">
					<input type="text" name="phone" placeholder="Телефон*" class="js-input" id="phone-6" required="required" />
				</div>
				<div class="form-row">
					<input type="text" name="city" placeholder="Город" class="js-input">
				</div>
				<div class="form-row">
					<textarea name="question" placeholder="Вопрос" class="js-input"></textarea>
				</div>				
				<div class="form-row">
					<input type="hidden" name="project" value="<?=$arResult["NAME"]?>" class="js-input" placeholder="Проект" />
					<input type="hidden" name="project-link" 
					value="http://urbancompany.ru/projects/<?=$arResult["NAME"]?>" class="js-input" placeholder="Ссылка" />
					<input type="button" name="button" class="button-modal js-submit"
					js-location="/res/res.php?f=question_send"
					value="ЗАДАТЬ ВОПРОС" />
				</div>

			</form>
		</div>
		<div class="text-small">
			Если Вам не перезвонили в течении 30 минут, наберите нас и мы предоставим Вам скидку.
		</div>	
	</div>
</div>


<div class="module module-download-example" id="form-download-example">
	<div class="module-form">
		<!-- <div class="close js-close">X</div> -->
		<div class="header">
			<h3>Скачать пример</h3>
			<p>
				*пример вышлем на email
			</p>
		</div>
		<div class="form">
			<form action="#" method="post" class="js-inputs" id="form-7">
				<div class="form-row">
					<input type="hidden" value="Скачать пример" name="header" class="js-input" />	
					<input type="text" name="name" placeholder="Имя*" class="js-input" id="name-7" required="required" />
				</div>
				<div class="form-row">
					<input type="text" name="phone" placeholder="Телефон*" class="js-input" id="phone-7" required="required" />
				</div>
				<div class="form-row">
					<input type="text" name="email" placeholder="Email" class="js-input">
				</div>
				<div class="form-row">
					<input type="text" name="city" placeholder="Город" class="js-input">
				</div>
				
				<div class="form-row">
					<input type="hidden" name="price-name-tarif" value="" class="js-input js-price-name-tarif" placeholder="" />
					<input type="hidden" name="project" value="<?=$arResult["NAME"]?>" class="js-input" placeholder="Проект" />
					<input type="hidden" name="project-link" 
					value="http://urbancompany.ru/projects/<?=$arResult["NAME"]?>" class="js-input" placeholder="Ссылка" />
					<input type="button" name="button" class="button-modal send_review js-submit" 
					js-location="/res/res.php?f=download_exaple"
					value="ОТПРАВИТЬ">
				</div>
			</form>
		</div>
		<div class="text-small">
			Если Вам не перезвонили в течении 30 минут, наберите нас и мы предоставим Вам скидку.
		</div>
	</div>
</div>