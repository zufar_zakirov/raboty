<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$arProp = $arResult["PROPERTIES"];
$url_image = CFile::GetPath($arProp['IMAGE_HEAD']['VALUE']);

$body_page_padding = 'body-page-padding';

?>
<?if($url_image):?>
	<div class="header-service">
		<div class="service-image" style="">
			<img src="<?=$url_image?>" class="bnr-seivice">
			<div class="b-head__service">
			    <div class="head-service__text--h1">	    	
			    	<div class="container">
			    		<?=$arResult["NAME"]?>
			    	</div>
			    </div>
			</div>
	    </div>
	</div>
	<?$body_page_padding='section';?>
<?endif;?>

<div class="<?=$body_page_padding?>">
	<div class="container">
		<div class="text">
			<?=$arResult["~DETAIL_TEXT"]?>
		</div>
	</div>
</div>

