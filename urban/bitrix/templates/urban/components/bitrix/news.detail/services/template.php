<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$arProp = $arResult["PROPERTIES"];
$url_image = CFile::GetPath($arProp['IMAGE_HEAD']['VALUE']);


?>
<div class="header-service">
	<div class="service-image" style="">
		<img src="<?=$url_image?>" class="bnr-seivice">
		<div class="b-head__service">
		    <div class="head-service__text--h1">
		    	<div class="container">
		    		<?=$arResult["NAME"]?>
		    	</div>
		    </div>
		</div>
    </div>
</div>

<div class="section">	
	<div class="container">
		<div class="text">
			<?=$arResult["~DETAIL_TEXT"]?>
		</div>
	</div>
</div>

