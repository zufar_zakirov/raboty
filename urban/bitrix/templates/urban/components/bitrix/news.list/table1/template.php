<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="section page-catalog">
		<div class="container">
			<div class="cart-list">



<?foreach($arResult["ITEMS"] as $arItem):?>
<? $PROP = $arItem["PROPERTIES"]; ?>
<?
$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
?>
<a class="cart-item" href="<?=$arItem["DETAIL_PAGE_URL"]?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>">

	<div class="cart-item--header">
		<h3 class="cart-item--header-h3"><?echo $arItem["NAME"]?></h3>
	</div>
	<?
	$img = CFile::ResizeImageGet( $PROP["PHOTOS"]["VALUE"][0]
        , Array("width" => 368, "height" => 320), BX_RESIZE_IMAGE_EXACT, false);
	?>
		
	<div class="cart-item--img">
		<img src="<?=$img['src']?>" class="cart-item--image" />
	</div>


	<div class="cart-item--text-hover">
		<div class="row-item-text-hover">
			<div class="icon-text-hover">
				<i class="icon-floors"></i>
				<div class="text-hover-field">
					Этажность
				</div>
			</div>
			<div class="floors-value text-hover-value"> <?=$PROP["FLOORS"]["VALUE"]?> </div>
		</div>

		<div class="row-item-text-hover">
			<div class="icon-size-hover icon-text-hover">
				<i class="icon-size"></i>
				<div class="text-hover-field">
					Габариты
				</div>
			</div>
			<div class="size-value text-hover-value"> <?=$PROP["SIZE_HOUSE"]["VALUE"]?>м </div>
		</div>

		<div class="row-item-text-hover">
			<div class="icon-text-hover">
				<i class="icon-rooms"></i>
				<div class="text-hover-field">
					Количество комнат
				</div>
			</div>
			<div class="rooms-value text-hover-value"> <?=$PROP["COUNT_ROOMS"]["VALUE"]?> </div>
		</div>
	</div>
	
		<div class="cart-item-footer">
			<div class="metric-area">
				<div class="icon-metric-area"></div>
				<div class="value-metric-area">
					<?=$PROP["TOTAL_AREA"]["VALUE"]?> м<sup>2</sup>
				</div>
			</div>
			<div class="price-item">
				<span class="price-item-number">
					<?=$PROP["PRICE_BASE"]["VALUE"]?>
				</span> 
				<span class="price-item-currency">&nbsp;руб.</span>
			</div>
		</div>
	</a>

<?endforeach;?>




		</div>
	</div>
</div>

<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>

