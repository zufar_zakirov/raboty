<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<?foreach($arResult["ITEMS"] as $arItem):?>
<? $PROP = $arItem["PROPERTIES"]; ?>

	
	<div class="col-6 col-md-3">
		<div class="company-number--item">
			<div class="company-number-data">
				<!-- <span class="ico-more"> > </span> -->
				<span class="number-more number-more-<?=$arItem['ID']?>" data-max="<?=$PROP["NUMBERS"]["VALUE"]?>">0</span>
			</div>
			<div class="company-number-text">
				<?=$PROP["TEXT"]["~VALUE"]["TEXT"]?>
			</div>		 
		</div>
	</div>
<?endforeach;?>