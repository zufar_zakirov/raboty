<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="headslider">
<?$n=1;?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<? if($n === 1){
		$class_active_slide = 'slide-active';
	}else{
		$class_active_slide = 'slide-none';
	}
	?>
	<div class="headslider__item slider__item_<?=$n?> <?=$class_active_slide?>"  
		style="background: url('<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>') no-repeat; background-size: cover;">
		<div class="b-headslider__content">
			<div class="headslider__content container bg-white-transparent-5">
				<div class="headslider__text">
					<?if($arItem["PROPERTIES"]['FIRST_HEAD']):?>
				    	<div class="headslider__text--h1"><?=$arItem["PROPERTIES"]['FIRST_HEAD']["VALUE"]?></div>
				    <?endif;?>
				    <?if($arItem["PROPERTIES"]['SECOND_HEAD']):?>
				    	<div class="headslider__text--h2"><?=$arItem["PROPERTIES"]['SECOND_HEAD']["VALUE"]?></div>
				    <?endif;?>	
				</div>
			</div>
		</div>
	</div>
	<?$n++;?>
<?endforeach;?>
</div>

<?if(false === true):?>
<div class="b-pointer-slide">
	<? $i = 1;?>
	<?foreach($arResult["ITEMS"] as $arItem):?>	
	<? if($i === 1){
		$class_active = 'active';
	}else{
		$class_active = '';
	}
	?>
		<div class="slider_pointer pointer_<?=$i?>" attr-class=".slider__item_<?=$i?>">
			<i class="icon-pointer <?=$class_active?>"></i>
		</div>
	<?$i++?>
<?endforeach;?>
</div>
<?endif;?>