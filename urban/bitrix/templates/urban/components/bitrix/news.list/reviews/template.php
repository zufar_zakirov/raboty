<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->addExternalCss("");
$this->setFrameMode(true);
?>
<div class="b-reviews--list slider">

<?foreach($arResult["ITEMS"] as $arItem):?>
<? $PROP = $arItem["PROPERTIES"]; 
	$url_image = CFile::GetPath($arProp['DEFAULT_IMAGE']['VALUE']);

	if(!$url_image){
		$url_image = 'img/icon_person.png';
	}
?>


	<div class="b-review-item">
		<div class="b-review-item--img">
			<div class="b-review-item--circle">
				<img src="<?=$url_image?>" class="review-item-img">
			</div>
		</div>

		<div class="b-review-item--text">
			<div class="review-text">
				<p>
					<?=$arItem['~DETAIL_TEXT']?>
				</p>
			</div>
			<div class="review-author text-right">
					<?=$arItem['NAME']?>
			</div>
		</div>
	</div>
<?endforeach;?>

</div>