<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="news-list">
<?$i = 1;?>
<?foreach($arResult["ITEMS"] as $arItem):?>
<?
$class_bg_grey = '';
if($i == 1){
	$class_img = 'left-new-block';
	$class_txt = 'right-new-block';
}else{
	$class_img = 'right-new-block';
	$class_txt = 'left-new-block';
	$class_bg_grey = 'section-bg-grey';
}
?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<div class="section-row-new-item <?=$class_bg_grey?>">
		<div class="container row-new-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
			<div class="new-img <?=$class_img?>">
				<?if(is_array($arItem["PREVIEW_PICTURE"])):?>
					<a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
						<img
							class="preview_picture"
							border="0"
							src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
							width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>"
							height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>"
							alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
							title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
							style="float:left" />			
					</a>
				<?endif?>
			</div>
			<div class="news-text <?=$class_txt?>">
				<div class="news-date-time"><?echo $arItem["DISPLAY_ACTIVE_FROM"]?></div>
				<div class="news-header">
					<a class="link-news-header" href="<?echo $arItem["DETAIL_PAGE_URL"]?>"><b><?echo $arItem["NAME"]?></b></a>
				</div>
				<p class="preview_text">
					<a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
						<?echo $arItem["PREVIEW_TEXT"];?>
					</a>
				</p>
			</div>
		</div>
	</div>
<?
$i++; 
if($i>2){
	$i = 1;
}
?>
<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
</div>
