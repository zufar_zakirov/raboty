<?php
if (!defined ('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
{
	die();
}
?>
<!DOCTYPE html>
<html lang="ru" class="page-index page-school">
<head>
	<?$APPLICATION->ShowProperty('AfterHeadOpen');?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><? $APPLICATION->ShowTitle(); ?></title>

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<!-- <link rel="stylesheet" href="/bitrix/templates/urban/css/lightbox.min.css" /> -->	
	<!-- <link rel="stylesheet" href="/bitrix/templates/urban/css/styles.css" /> -->

	<?
	$APPLICATION->ShowHead();
	$APPLICATION->ShowProperty('MetaOG');
	$APPLICATION->ShowProperty('BeforeHeadClose');
	?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="stylesheet" href="/bitrix/templates/urban/css/mobile.css?v=3" />
	<link rel="stylesheet" href="/bitrix/templates/urban/css/slick.css" />
	<link rel="stylesheet" href="/bitrix/templates/urban/css/slick-theme.css?v=2" />
	<link rel="stylesheet" href="/bitrix/templates/urban/css/jquery.fancybox.min.css" />
	<script type="text/javascript" src="https://analytics.alloka.ru/script/dec3a6f7a1fef8ea"></script>
	<link rel="shortcut icon" href="/img/favicon.ico" />
	<meta name="yandex-verification" content="50670a4b0ece418e" />
</head>
<body>
	<div class="body-page">
	<?$APPLICATION->ShowPanel()?>
<div class="top-row-mobile">
	<div class="mobile m-logo"><a href="/" class="logo-link"></a></div>
	<div class="top js-top">
		<div class="container"> 
			<div class="row">
				<!-- <div class="col-2 top-vk-black"></div> -->				
				<!-- <div class="col-12 col-lg-3 b-call-back">
					<div class="call-back js-module-call-back">обратный звонок</div>
				</div> -->				
				<div class="col-12 col-lg-3 top-email">
					<!-- <div class="email"> -->
						<!-- <a href="mailto:info@urbancompany.ru" class="email-link">info@urbancompany.ru</a> -->
						<!-- <div class="call-back js-module-write-us">Написать</div> -->
						<div class="call-back js-module-write-us" data-fancybox data-src="#form-write-us">написать</div>
					<!-- </div> -->
				</div>
				<div class="col-12 col-lg-4 top-phone text-right">
					<div class="phone phone_alloka">8 800 300 57 84 <div class="small header-small">(звонок бесплатный)</div></div>
				</div>
				<div class="col-12 col-lg-5 top-email">
					<div class="phone">8 958 844 01 46 <div class="small header-small">(whatsapp, viber, telegram)</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>
<div class="mobile mobile-top-row">
	<div class="mobile-menu"><i class="icon-menu-btn js-mobile-menu-btn"></i></div>
	<!-- <div class="m-header-text">Проектирование домов по всей России</div> -->
</div>
	<nav class="menu-top bg-white-transparent-6">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 m-none">
					<div class="logo">
						<a href="/" class="logo--link"></a>
					</div>
				</div>
				<div class="col-12 col-lg-9 align-items-center d-xl-flex justify-content-lg-end b-menu">
					<div class="mobile m-menu-header">меню</div>
					<div class="b-menu-top">
						<?$APPLICATION->IncludeComponent(
							"bitrix:menu", 
							"urban_menu",
							array(
								"ALLOW_MULTI_SELECT" => "N",
								"CHILD_MENU_TYPE" => "left",
								"DELAY" => "N",
								"MAX_LEVEL" => "2",
								"MENU_CACHE_GET_VARS" => array(
								),
								"MENU_CACHE_TIME" => "3600",
								"MENU_CACHE_TYPE" => "N",
								"MENU_CACHE_USE_GROUPS" => "Y",
								"ROOT_MENU_TYPE" => "top",
								"USE_EXT" => "N",
								"COMPONENT_TEMPLATE" => "urban_menu",
								"MENU_THEME" => "site"
							),
							false
						);?>
					</div>
				</div>
			</div>			
		</div>
	</nav>